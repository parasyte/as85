Addressing modes:
boffset:			branch offset (PC + 2 + boffset)
cc:				condition code (see below)
imm3:				3-bit immediate value
imm8:				8-bit immediate value
imm16:				16-bit immediate value
joffset:			jump offset (absolute 16-bit)
n:				zero page address (0x0000 - 0x00FF)
nn:				absolute address (0x0000 - 0xFFFF)
p:				port (p10 - p17)
pcr:				PC-relative (PC + pcr)
r:				register
R:				register file
rr:				register pair (encoded: IJK = 0xKIJ0)
RR:				register file pair
soffset:			short call offset (bank + coffset)
xx:				absolute address (0xFF00 - 0xFFFF)
+:				additional register
@:				indirect

Condition codes:
f:				conditional: false
lt:				conditional: less than
le:				conditional: less or equal
ule:				conditional: unsigned less or equal
ov:				conditional: overflow
mi:				conditional: minus
z:				conditional: zero
c:				conditional: carry
t:				conditional: true
ge:				conditional: greater or equal
gt:				conditional: greater than
ugt:				conditional: unsigned greater than
nov:				conditional: not overflow
pl:				conditional: plus
nz:				conditional: not zero
nc:				conditional: not carry





Instruction map:
clr	R			00000000 RRRRRRRR
neg	R			00000001 RRRRRRRR
com	R			00000010 RRRRRRRR
rr	R			00000011 RRRRRRRR
rl	R			00000100 RRRRRRRR
rrc	R			00000101 RRRRRRRR
rlc	R			00000110 RRRRRRRR
srl	R			00000111 RRRRRRRR
inc	R			00001000 RRRRRRRR
dec	R			00001001 RRRRRRRR
sra	R			00001010 RRRRRRRR
sll	R			00001011 RRRRRRRR
da	R			00001100 RRRRRRRR
swap	R			00001101 RRRRRRRR
push	R			00001110 RRRRRRRR
pop	R			00001111 RRRRRRRR

cmp	r,r			00010000 00rrr+++
add	r,r			00010001 00rrr+++
sub	r,r			00010010 00rrr+++
adc	r,r			00010011 00rrr+++
sbc	r,r			00010100 00rrr+++
and	r,r			00010101 00rrr+++
or	r,r			00010110 00rrr+++
xor	r,r			00010111 00rrr+++

incw	RR			00011000 RRRRRRR0
decw	RR			00011001 RRRRRRR0
pushw	RR			00011110 RRRRRRR0
popw	RR			00011111 RRRRRRR0

clr	@r			00011010 00rrr000
neg	@r			00011010 00rrr001
com	@r			00011010 00rrr010
rr	@r			00011010 00rrr011
rl	@r			00011010 00rrr100
rrc	@r			00011010 00rrr101
rlc	@r			00011010 00rrr110
srl	@r			00011010 00rrr111
inc	@r			00011011 00rrr000
dec	@r			00011011 00rrr001
sra	@r			00011011 00rrr010
sll	@r			00011011 00rrr011
da	@r			00011011 00rrr100
swap	@r			00011011 00rrr101
push	@r			00011011 00rrr110
pop	@r			00011011 00rrr111

bclr	xx,#imm3		00011100 00000iii, xx
bset	xx,#imm3		00011101 00000iii, xx

bclr	n(r),#imm3		00011100 00rrriii, imm8
bset	n(r),#imm3		00011101 00rrriii, imm8

cmp	r,@r			00100000 00rrr+++
add	r,@r			00100001 00rrr+++
sub	r,@r			00100010 00rrr+++
adc	r,@r			00100011 00rrr+++
sbc	r,@r			00100100 00rrr+++
and	r,@r			00100101 00rrr+++
or	r,@r			00100110 00rrr+++
xor	r,@r			00100111 00rrr+++
mov	r,@r			00101000 00rrr+++
mov	@r,r			00101001 00rrr+++

bbc	xx,#imm3,pcr		00101010 00000iii, xx, pcr
bbs	xx,#imm3,pcr		00101011 00000iii, xx, pcr

bbc	n(r),#imm3,pcr		00101010 00rrriii, imm8, pcr
bbs	n(r),#imm3,pcr		00101011 00rrriii, imm8, pcr

exts	RR			00101100 RRRRRRR0

dm	#imm8			00101101 imm8				(???)
mov	ps0,#imm8		00101110 imm8

btst	R,#imm8			00101111 RRRRRRRR, imm8

cmp	r,(r)+			00100000 01rrr+++
add	r,(r)+			00100001 01rrr+++
sub	r,(r)+			00100010 01rrr+++
adc	r,(r)+			00100011 01rrr+++
sbc	r,(r)+			00100100 01rrr+++
and	r,(r)+			00100101 01rrr+++
or	r,(r)+			00100110 01rrr+++
xor	r,(r)+			00100111 01rrr+++
mov	r,(r)+			00101000 01rrr+++
mov	(r)+,r			00101001 01rrr+++

cmp	r,n(r)			00100000 10rrr+++, nnnnnnnn
add	r,n(r)			00100001 10rrr+++, nnnnnnnn
sub	r,n(r)			00100010 10rrr+++, nnnnnnnn
adc	r,n(r)			00100011 10rrr+++, nnnnnnnn
sbc	r,n(r)			00100100 10rrr+++, nnnnnnnn
and	r,n(r)			00100101 10rrr+++, nnnnnnnn
or	r,n(r)			00100110 10rrr+++, nnnnnnnn
xor	r,n(r)			00100111 10rrr+++, nnnnnnnn
mov	r,n(r)			00101000 10rrr+++, nnnnnnnn
mov	n(r),r			00101001 10rrr+++, nnnnnnnn

cmp	r,-(r)			00100000 11rrr+++
add	r,-(r)			00100001 11rrr+++
sub	r,-(r)			00100010 11rrr+++
adc	r,-(r)			00100011 11rrr+++
sbc	r,-(r)			00100100 11rrr+++
and	r,-(r)			00100101 11rrr+++
or	r,-(r)			00100110 11rrr+++
xor	r,-(r)			00100111 11rrr+++
mov	r,-(r)			00101000 11rrr+++
mov	-(r),r			00101001 11rrr+++

cmp	r,@rr			00110000 00rrr+++
add	r,@rr			00110001 00rrr+++
sub	r,@rr			00110010 00rrr+++
adc	r,@rr			00110011 00rrr+++
sbc	r,@rr			00110100 00rrr+++
and	r,@rr			00110101 00rrr+++
or	r,@rr			00110110 00rrr+++
xor	r,@rr			00110111 00rrr+++
mov	r,@rr			00111000 00rrr+++
mov	@rr,r			00111001 00rrr+++
movw	rr,@rr			00111010 00rrr+++
movw	@rr,rr			00111011 00rrr+++
movw	rr,rr			00111100 00rrr+++

dm	R			00111101 RRRRRRRR			(???)

jmp	@rr			00111110 00000+++
call	@rr			00111111 00000+++

cmp	r,(rr)+			00110000 01rrr+++
add	r,(rr)+			00110001 01rrr+++
sub	r,(rr)+			00110010 01rrr+++
adc	r,(rr)+			00110011 01rrr+++
sbc	r,(rr)+			00110100 01rrr+++
and	r,(rr)+			00110101 01rrr+++
or	r,(rr)+			00110110 01rrr+++
xor	r,(rr)+			00110111 01rrr+++
mov	r,(rr)+			00111000 01rrr+++
mov	(rr)+,r			00111001 01rrr+++
movw	rr,(rr)+		00111010 01rrr+++
movw	(rr)+,rr		00111011 01rrr+++

jmp	@nn(r)			00111110 01rrr000, imm16
call	@nn(r)			00111111 01rrr000, imm16

cmp	r,nn(rr)		00110000 10rrr+++, imm16
add	r,nn(rr)		00110001 10rrr+++, imm16
sub	r,nn(rr)		00110010 10rrr+++, imm16
adc	r,nn(rr)		00110011 10rrr+++, imm16
sbc	r,nn(rr)		00110100 10rrr+++, imm16
and	r,nn(rr)		00110101 10rrr+++, imm16
or	r,nn(rr)		00110110 10rrr+++, imm16
xor	r,nn(rr)		00110111 10rrr+++, imm16
mov	r,nn(rr)		00111000 10rrr+++, imm16
mov	nn(rr),r		00111001 10rrr+++, imm16
movw	rr,nn(rr)		00111010 10rrr+++, imm16
movw	nn(rr),rr		00111011 10rrr+++, imm16

cmp	r,-(rr)			00110000 11rrr+++
add	r,-(rr)			00110001 11rrr+++
sub	r,-(rr)			00110010 11rrr+++
adc	r,-(rr)			00110011 11rrr+++
sbc	r,-(rr)			00110100 11rrr+++
and	r,-(rr)			00110101 11rrr+++
or	r,-(rr)			00110110 11rrr+++
xor	r,-(rr)			00110111 11rrr+++
mov	r,-(rr)			00111000 11rrr+++
mov	-(rr),r			00111001 11rrr+++
movw	rr,-(rr)		00111010 11rrr+++
movw	-(rr),rr		00111011 11rrr+++

cmp	R,R			01000000 ++++++++, RRRRRRRR
add	R,R			01000001 ++++++++, RRRRRRRR
sub	R,R			01000010 ++++++++, RRRRRRRR
adc	R,R			01000011 ++++++++, RRRRRRRR
sbc	R,R			01000100 ++++++++, RRRRRRRR
and	R,R			01000101 ++++++++, RRRRRRRR
or	R,R			01000110 ++++++++, RRRRRRRR
xor	R,R			01000111 ++++++++, RRRRRRRR
mov	R,R			01001000 ++++++++, RRRRRRRR

call	nn			01001001 imm16

movw	RR,RR			01001010 +++++++0, RRRRRRR0
movw	RR,#imm16		01001011 RRRRRRR0, imm16

mult	RR,R			01001100 ++++++++, RRRRRRR0
mult	RR,#imm8		01001101 imm8, RRRRRRR0

bmov	bf,R,#imm3		01001110 00000iii, RRRRRRRR
bmov	R,#imm3,bf		01001110 01000iii, RRRRRRRR
bcmp	bf,R,#imm3		01001111 00000iii, RRRRRRRR
band	bf,R,#imm3		01001111 01000iii, RRRRRRRR
bor	bf,R,#imm3		01001111 10000iii, RRRRRRRR
bxor	bf,R,#imm3		01001111 11000iii, RRRRRRRR

cmp	R,#imm8			01010000 imm8, RRRRRRRR
add	R,#imm8			01010001 imm8, RRRRRRRR
sub	R,#imm8			01010010 imm8, RRRRRRRR
adc	R,#imm8			01010011 imm8, RRRRRRRR
sbc	R,#imm8			01010100 imm8, RRRRRRRR
and	R,#imm8			01010101 imm8, RRRRRRRR
or	R,#imm8			01010110 imm8, RRRRRRRR
xor	R,#imm8			01010111 imm8, RRRRRRRR
mov	R,#imm8			01011000 imm8, RRRRRRRR

div	RR,RR			01011100 +++++++0, RRRRRRR0
div	RR,#imm8		01011101 imm8, RRRRRRR0

movm	R,#imm8,R		01011110 RRRRRRRR, imm8, ++++++++
movm	R,#imm8,#imm8		01011111 RRRRRRRR, imm8, imm8+

cmpw	RR,RR			01100000 +++++++0, RRRRRRR0
addw	RR,RR			01100001 +++++++0, RRRRRRR0
subw	RR,RR			01100010 +++++++0, RRRRRRR0
adcw	RR,RR			01100011 +++++++0, RRRRRRR0
sbcw	RR,RR			01100100 +++++++0, RRRRRRR0
andw	RR,RR			01100101 +++++++0, RRRRRRR0
orw	RR,RR			01100110 +++++++0, RRRRRRR0
xorw	RR,RR			01100111 +++++++0, RRRRRRR0

cmpw	RR,#imm16		01101000 RRRRRRR0, imm16
addw	RR,#imm16		01101001 RRRRRRR0, imm16
subw	RR,#imm16		01101010 RRRRRRR0, imm16
adcw	RR,#imm16		01101011 RRRRRRR0, imm16
sbcw	RR,#imm16		01101100 RRRRRRR0, imm16
andw	RR,#imm16		01101101 RRRRRRR0, imm16
orw	RR,#imm16		01101110 RRRRRRR0, imm16
xorw	RR,#imm16		01101111 RRRRRRR0, imm16

dbnz	r,boffset		01110rrr boffset

movw	rr,#imm16		01111rrr imm16

bbc	R,#imm3,boffset		10000iii RRRRRRRR, boffset
bbs	R,#imm3,boffset		10001iii RRRRRRRR, boffset

jmp	cc,nn			1001cccc imm16

bclr	R,#imm3			10100iii RRRRRRRR
bset	R,#imm3			10101iii RRRRRRRR

mov	r,R			10110rrr RRRRRRRR
mov	R,r			10111rrr RRRRRRRR

mov	r,#imm8			11000rrr imm8
mov	p,#imm8			11001ppp imm8

br	cc,boffset		1101cccc boffset

cals	soffset			1110ssss sssssss

stop				11110000
halt				11110001
ret				11111000
iret				11111001
clrc				11111010
comc				11111011
setc				11111100
ei				11111101
di				11111110
nop				11111111




Available addressing modes:
clr:
	R
	@r
mov:
	r,@r
	@r,r
	ps0,#imm8
	r,(r)+
	(r)+,r
	r,n(r)
	n(r),r
	r,-(r)
	-(r),r
	r,@rr
	@rr,r
	r,(rr)+
	(rr)+,r
	r,nn(rr)
	nn(rr),r
	r,-(rr)
	-(rr),r
	R,R
	R,#imm8
	r,R
	R,r
	r,#imm8
	p,#imm8

