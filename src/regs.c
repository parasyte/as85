/* as85 - Assembler for Sharp sm8521 CPU
 * Sharp sm8521 register routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <string.h>
#include "regs.h"


char *regs[NUM_REGS] = {
    NULL,           // 0
    NULL,           // 1
    NULL,           // 2
    NULL,           // 3
    NULL,           // 4
    NULL,           // 5
    NULL,           // 6
    NULL,           // 7
    NULL,           // 8
    NULL,           // 9
    NULL,           // 10
    NULL,           // 11
    NULL,           // 12
    NULL,           // 13
    NULL,           // 14
    NULL,           // 15
    "ie0",          // 16
    "ie1",          // 17
    "ir0",          // 18
    "ir1",          // 19
    "p0",           // 20
    "p1",           // 21
    "p2",           // 22
    "p3",           // 23
    NULL,           // 24
    "sys",          // 25
    "ckc",          // 26
    NULL,           // 27
    "sph",          // 28
    "spl",          // 29
    "ps0",          // 30
    "ps1",          // 31
    "p0c",          // 32
    "p1c",          // 33
    "p2c",          // 34
    "p3c",          // 35
    "mmu0",         // 36
    "mmu1",         // 37
    "mmu2",         // 38
    "mmu3",         // 39
    "mmu4",         // 40
    NULL,           // 41
    NULL,           // 42
    "uartt",        // 43
    "uartr",        // 44
    "uarts",        // 45
    "uartc",        // 46
    NULL,           // 47
    "lcc",          // 48
    "lch",          // 49
    "lcv",          // 50
    NULL,           // 51
    "dmc",          // 52
    "dmx1",         // 53
    "dmy1",         // 54
    "dmdx",         // 55
    "dmdy",         // 56
    "dmx2",         // 57
    "dmy2",         // 58
    "dmpl",         // 59
    "dmbr",         // 60
    "dmvp",         // 61
    NULL,           // 62
    NULL,           // 63
    "sgc",          // 64
    NULL,           // 65
    "sg0l",         // 66
    NULL,           // 67
    "sg1l",         // 68
    NULL,           // 69
    "sg0th",        // 70
    "sg0tl",        // 71
    "sg1th",        // 72
    "sg1tl",        // 73
    "sg2l",         // 74
    NULL,           // 75
    "sg2th",        // 76
    "sg2tl",        // 77
    "sgda",         // 78
    NULL,           // 79
    "tm0c",         // 80
    "tm0d",         // 81
    "tm1c",         // 82
    "tm1d",         // 83
    "clkd",         // 84
    NULL,           // 85
    NULL,           // 86
    NULL,           // 87
    NULL,           // 88
    NULL,           // 89
    NULL,           // 90
    NULL,           // 91
    NULL,           // 92
    NULL,           // 93
    "wdt",          // 94
    "wdtc",         // 95
    "sg0w0",        // 96
    "sg0w1",        // 97
    "sg0w2",        // 98
    "sg0w3",        // 99
    "sg0w4",        // 100
    "sg0w5",        // 101
    "sg0w6",        // 102
    "sg0w7",        // 103
    "sg0w8",        // 104
    "sg0w9",        // 105
    "sg0w10",       // 106
    "sg0w11",       // 107
    "sg0w12",       // 108
    "sg0w13",       // 109
    "sg0w14",       // 110
    "sg0w15",       // 111
    "sg1w0",        // 112
    "sg1w1",        // 113
    "sg1w2",        // 114
    "sg1w3",        // 115
    "sg1w4",        // 116
    "sg1w5",        // 117
    "sg1w6",        // 118
    "sg1w7",        // 119
    "sg1w8",        // 120
    "sg1w9",        // 121
    "sg1w10",       // 122
    "sg1w11",       // 123
    "sg1w12",       // 124
    "sg1w13",       // 125
    "sg1w14",       // 126
    "sg1w15",       // 127
};

char *regs_w[NUM_REGS / 2] = {
    NULL,           // 0
    NULL,           // 2
    NULL,           // 4
    NULL,           // 6
    NULL,           // 8
    NULL,           // 10
    NULL,           // 12
    NULL,           // 14
    "ie",           // 16
    "ir",           // 18
    "p0_w",         // 20
    "p2_w",         // 22
    NULL,           // 24
    "ckc_w",        // 26
    "sp",           // 28
    "ps",           // 30
    "p0c_w",        // 32
    "p2c_w",        // 34
    "mmu0_w",       // 36
    "mmu2_w",       // 38
    "mmu4_w",       // 40
    NULL,           // 42
    "uartr_w",      // 44
    "uartc_w",      // 46
    "lcc_w",        // 48
    "lcv_w",        // 50
    "dmc_w",        // 52
    "dmy1_w",       // 54
    "dmdy_w",       // 56
    "dmy2_w",       // 58
    "dmbr_w",       // 60
    NULL,           // 62
    "sgc_w",        // 64
    "sg0l_w",       // 66
    "sg1l_w",       // 68
    "sg0t",         // 70
    "sg1t",         // 72
    "sg2l_w",       // 74
    "sg2t",         // 76
    "sgda_w",       // 78
    "tm0c_w",       // 80
    "tm1c_w",       // 82
    "clkd_w",       // 84
    NULL,           // 86
    NULL,           // 88
    NULL,           // 90
    NULL,           // 92
    "wdt_w",        // 94
    "sg0w0_w",      // 96
    "sg0w2_w",      // 98
    "sg0w4_w",      // 100
    "sg0w6_w",      // 102
    "sg0w8_w",      // 104
    "sg0w10_w",     // 106
    "sg0w12_w",     // 108
    "sg0w14_w",     // 110
    "sg1w0_w",      // 112
    "sg1w2_w",      // 114
    "sg1w4_w",      // 116
    "sg1w6_w",      // 118
    "sg1w8_w",      // 120
    "sg1w10_w",     // 122
    "sg1w12_w",     // 124
    "sg1w14_w",     // 126
};
