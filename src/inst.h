/* as85 - Assembler for Sharp sm8521 CPU
 * Sharp sm8521 instruction routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __INST_H__
#define __INST_H__

#include <stdint.h>


// Address mode patterns
/*
 Pattern formats:
 %c  Condition code (not yet available)
 %e  Expression (not yet available)
 %i  Matches a port register
 %n  Matches a literal number and places it in output (to be [mostly] superseded by %e)
 %p  Matches a register pair value and places it in output
 %r  Matches a register value and places it in output
 %s  Matches one or more printable characters enclosed in double quotes ("...")
   --Returns a pointer to string.
 %t  Matches one printable character
 %w  Matches zero or more characters of white space
 %P  Matches a register file pair value and places it in output
 %R  Matches a register file value and places it in output

 Everything else is taken literally
*/
#define AMP_separator               "%w,%w"
#define AMP_reg                     "%r"
#define AMP_reg_pair                "%p"
#define AMP_reg_file                "%R"
#define AMP_reg_file_pair           "%P"
#define AMP_reg_indirect            "@%r"
#define AMP_reg_indirect_inc        "(%w%r%w)+"
#define AMP_reg_indirect_dec        "-(%w%r%w)"
#define AMP_reg_index               "%n%w(%w%r%w)"
#define AMP_reg_pair_indirect       "@%p"
#define AMP_reg_pair_indirect_inc   "(%w%p%w)+"
#define AMP_reg_pair_indirect_dec   "-(%w%p%w)"
#define AMP_reg_pair_index          "%n%w(%w%p%w)"
#define AMP_immediate               "#%n"
#define AMP_direct                  "%n"
#define AMP_direct_indirect         "@%n"
#define AMP_port                    "%i"
#define AMP_ps0                     "ps0"
#define AMP_bf                      "bf"


// Number of instructions in each instruction type
#define NUM_TYPE0       10
#define NUM_TYPE1       16
#define NUM_TYPE2       8
#define NUM_TYPE3       8
#define NUM_TYPE4       5
#define NUM_TYPE5       2
#define NUM_TYPE6       4
#define NUM_NOTYPE      7


// Number of instructions supported
#define NUM_DINST       4
#define NUM_PINST       0
#define NUM_INST        (NUM_TYPE0 + NUM_TYPE1 + NUM_TYPE2 + NUM_TYPE3 + NUM_TYPE4 + NUM_TYPE5 + NUM_TYPE6 + NUM_NOTYPE + NUM_DINST + NUM_PINST)


// Instruction map
typedef struct {
    char *name;
    int (*func)(char *, const uint8_t *, int *, uint8_t *);
    const uint8_t *bytecode;
} INST;

extern INST inst[NUM_INST];


// Instruction handlers
int inst_type0(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // stop, halt, ret, iret, clrc, comc, setc, ei, di, nop
int inst_type1(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // clr, neg, com, rr, rl, rrc, rlc, srl, inc, dec, sra, sll, da, swap, push, pop
int inst_type2(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // cmp, add, sub, adc, sbc, and, or, xor
int inst_type3(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // cmpw, addw, subw, adcw, sbcw, andw, orw, xorw
int inst_type4(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // incw, decw, pushw, popw, exts
int inst_type5(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // bclr, bset
int inst_type6(char *args, const uint8_t *bytecode, int *size, uint8_t *out); // bcmp, band, bor, bxor
int inst_bmov(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_btst(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_div(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_mov(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_movm(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_movw(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int inst_mult(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int dinst_align(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int dinst_db(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int dinst_dw(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int dinst_dd(char *args, const uint8_t *bytecode, int *size, uint8_t *out);
int dinst_org(char *args, const uint8_t *bytecode, int *size, uint8_t *out);

#endif // __INST_H__
