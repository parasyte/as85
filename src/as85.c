/* as85 - Assembler for Sharp sm8521 CPU
 * Main program routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <stdlib.h>
#include "as85.h"
#include "asm.h"
#include "error.h"
#include "symbol.h"


char *err_string[] = {
    "OK",
    "Parse error",
    "Invalid arguments supplied",
    "Invalid address mode",
    "Unable to open file",
    "Unable to allocate memory",
    "Symbol already exists",
    "Could not resolve symbol",
    "General error",
    "Warning",
};


STATE *current_state = NULL;

STATE *get_current_state() {
    return current_state;
}

void set_current_state(STATE *state) {
    current_state = state;
}



static void cleanup() {

#ifdef DBG
    printf("Clean up...\n");
#endif // DBG

    delete_symbol_table(&symtab);
    delete_errors();
}

int main(int argc, char **argv) {
    STATE state;
    int ret;

    // Initialize initial state
    state.pc = 0;
    set_current_state(&state);

    atexit(cleanup);

    if (argc != 2) {
        printf("Usage: as85 <filename>\n");
        return 1;
    }

    ret = assemble(argv[1]);
    printf("assemble() returned %d: %s\n", ret, err_string[ret]);

    return 0;
}
