/* as85 - Assembler for Sharp sm8521 CPU
 * Assembler routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __ASM_H__
#define __ASM_H__

#include <stddef.h>
#include <stdint.h>

#ifdef __WIN32__
#define strnicmp _strnicmp
#else // __WIN32__
#include <strings.h>
#define strnicmp strncasecmp
#endif // __WIN32__


// Object format
typedef struct _CHUNK {
    uint32_t length;
    uint32_t alloc;
    uint8_t *data;
} CHUNK;

typedef struct _OBJECT {
    uint32_t length;
    uint32_t alloc;
    CHUNK **chunks;
} OBJECT;


// String checkers
int islabel(char *arg);
int str_escape_char(char *out, char arg);

// Tokenization (lexical analyzer)
int chk_pattern(char *args, char *cmp, size_t *out);

// Main
int assemble(char *filename);

#endif // __ASM_H__
