/* as85 - Assembler for Sharp sm8521 CPU
 * Extended string routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2004,2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "xstring.h"


// Upper case routine
//  Returns number of characters modified
int str_ucase(char *str) {
    size_t i = 0;
    int j = 0;

    while (i < strlen(str)) {
        if ((str[i] >= 'a') && (str[i] <= 'z')) {
            str[i] &= ~0x20;
            j++;
        }
        i++;
    }
    return j;
}


// Lower case routine
//  Returns number of characters modified
int str_lcase(char *str) {
    size_t i = 0;
    int j = 0;

    while (i < strlen(str)) {
        if ((str[i] >= 'A') && (str[i] <= 'Z')) {
            str[i] |= 0x20;
            j++;
        }
        i++;
    }
    return j;
}


// White space-trimming routine
//  Removes whitespace from left side of string, depending on the flags set (See STRIP_x definitions in xstring.h)
//  Returns number of characters removed
int str_ltrim(char *str, int flags) {
    int i = 0;

    while (str[0]) {
        if (!ISWHITESPACE(str[0])) break;

        if ((flags & STRIP_SP) && (str[0] == ' ')) {
            i++;
            strcpy(str, &str[1]);
        }
        if ((flags & STRIP_TAB) && (str[0] == '\t')) {
            i++;
            strcpy(str, &str[1]);
        }
        if ((flags & STRIP_CR) && (str[0] == '\r')) {
            i++;
            strcpy(str, &str[1]);
        }
        if ((flags & STRIP_LF) && (str[0] == '\n')) {
            i++;
            strcpy(str, &str[1]);
        }
    }
    return i;
}


// White space-trimming routine
//  Removes whitespace from right side of string, depending on the flags set (See STRIP_x definitions in xstring.h)
//  Returns number of characters removed
int str_rtrim(char *str, int flags) {
    int i = 0;
    int len;

    while ((len = strlen(str))) {
        len--;
        if (!ISWHITESPACE(str[len])) break;

        if ((flags & STRIP_SP) && (str[len] == ' ')) {
            i++;
            str[len] = 0;
        }
        if ((flags & STRIP_TAB) && (str[len] == '\t')) {
            i++;
            str[len] = 0;
        }
        if ((flags & STRIP_CR) && (str[len] == '\r')) {
            i++;
            str[len] = 0;
        }
        if ((flags & STRIP_LF) && (str[len] == '\n')) {
            i++;
            str[len] = 0;
        }
    }
    return i;
}


// White space-trimming routine
//  Removes whitespace from left and right sides of string, depending on the flags set (See STRIP_x definitions in xstring.h)
//  Returns number of characters removed
int str_trim(char *str, int flags) {
    return (str_ltrim(str, flags) + str_rtrim(str, flags));
}


// White space-stripping routine
//  Removes whitespace depending on the flags set (See STRIP_x definitions in xstring.h)
//  Returns number of characters removed, or -1 on error
int str_strip(char *str, int flags) {
    size_t i = 0;
    int j = 0;
    char *buffer;
    char chr;

    if (!strlen(str)) return -1;
    if (!(flags & (STRIP_SP | STRIP_TAB | STRIP_CR | STRIP_LF))) return -1;
    if (!(buffer = (char*)malloc(strlen(str) + 1))) return -1;
    while (i < strlen(str)) {
        chr = str[i++];
        if ((flags & STRIP_SP) && (chr == ' ')) chr = 0;
        if ((flags & STRIP_TAB) && (chr == '\t')) chr = 0;
        if ((flags & STRIP_CR) && (chr == '\r')) chr = 0;
        if ((flags & STRIP_LF) && (chr == '\n')) chr = 0;

        if (chr) buffer[j++] = chr;
    }
    buffer[j] = 0;
    strcpy(str, buffer);
    free(buffer);

    return (i - j);
}


// Character replacement routine
//  Replaces all instances of 'search' with 'replace'
//  Returns number of characters modified
int chr_replace(char *str, char search, char replace) {
    size_t i = 0;
    int j = 0;

    while (i < strlen(str)) {
        if (str[i] == search) {
            str[i] = replace;
            j++;
        }
        i++;
    }
    return j;
}


// Sub-String replacement routine
//  Replaces all instances of 'search' with 'replace'
//  Returns number of sub-strings modified, or -1 on error
int str_replace(char *str, char *search, char *replace) {
    size_t i = 0;
    int j = 0;
    int search_len;
    int replace_len;
    char *buffer;

    search_len = strlen(search);
    replace_len = strlen(replace);
    if ((!strlen(str)) || (!search_len)) return -1; // Note: allow *replace to have a length of zero!
    if (!(buffer = (char*)malloc(strlen(str) + 1))) return -1;
    while (i < strlen(str)) {
        if (!strncmp(&str[i], search, search_len)) {
            if (replace_len) memcpy(&buffer[j], replace, replace_len);
            i += search_len;
            j += replace_len;
        }
        else buffer[j++] = str[i++];
    }
    buffer[j] = 0;
    strcpy(str, buffer);
    free(buffer);

    return j;
}


// Sub-String search routine, ignoring everything between single and double quotes
//  Returns character position of first located sub-string, or -1 on error
int str_find_parsed(char *str, char *search) {
    size_t i;
    int is_string = 0;
    char *buffer;
    char *ret;

    // Create a buffer replacing everything inside is_strings with ?
    if (!(buffer = (char*)malloc(strlen(str) + 1))) return -1;
    strcpy(buffer, str);
    for (i = 0; i < strlen(buffer); i++) {
        if      (((is_string == 0) || (is_string == 1)) && (buffer[i] == '"'))  is_string ^= 1; // Double quotes
        else if (((is_string == 0) || (is_string == 2)) && (buffer[i] == '\'')) is_string ^= 2; // Single quotes
        else if (is_string) {
            if (buffer[i] == '\\') buffer[i++] = '?'; // Escape characters come in twos
            buffer[i] = '?';
        }
    }
    ret = strstr(buffer, search);
    i = (ret - buffer);
    free(buffer);

    if (!ret) return -1;
    return i;
}
