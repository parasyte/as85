/* as85 - Assembler for Sharp sm8521 CPU
 * Assembler routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "as85.h"
#include "asm.h"
#include "error.h"
#include "inst.h"
#include "regs.h"
#include "scan.h"
#include "symbol.h"
#include "xstring.h"


// String checkers
int islabel(char *arg) {
    int i;
    int len;

    len = (strlen(arg) - 1);
    if (arg[len] != ':') return SYMTYPE_NONE;

    len--;
    for (i = 0; i < len; i++) {
        if (!ISWORD(arg[i])) return SYMTYPE_NONE; // Only allow alphanumeric and underscore characters
    }

    if (ISNUM(arg[0])) return SYMTYPE_LOCAL_LABEL; // FIXME: Local labels should be complete numbers
    return SYMTYPE_LABEL;
}

int str_escape_char(char *out, char arg) {
    switch (arg) {
        case '\\':
        case '"':
        case '\'':
            *out = arg;
            break;

        case '0':
            *out = '\0';
            break;

        case 'n':
            *out = '\n';
            break;

        case 'r':
            *out = '\r';
            break;

        case 't':
            *out = '\t';
            break;

//        case 'x':
//            break;

        default:
            return ERR_PARSE;
    }

    return ERR_NONE;
}



// Tokenization (lexical analyzer)
int chk_pattern(char *args, char *cmp, size_t *out) {
    int i = 0;
    int j;
    int c = 0;
    int len;
    int ret;
    int port;
    static char str[MAX_ARG_SIZE];
    char buffer[MAX_ARG_SIZE];
    char *strp = str;

    strcpy(buffer, args);
    while ((buffer[i] != 0) || (cmp[c] != 0)) {
        if (cmp[c] == '%') {
            // Parse operators
            c++;
            switch (cmp[c++]) {
                case 'n': // Number
                    if ((ret = scan_number(&buffer[i], out)) <= 0) return ERR_ARGS;
                    out++; // Advance output pointer
                    i += ret;
                    break;

                case 'p': // Register Pair
                    if ((buffer[i] == 'r') && (buffer[i + 1] == 'r')) {
                        i += 2;
                        if ((ret = scan_regval(&buffer[i], out)) <= 0) return ERR_ARGS;
                        if (*out & 1) return ERR_ARGS;
                        if (*out >= 16) return ERR_ARGS;
                        *out = ((*out | (*out >> 3)) & 7); // Compress data: Shift MSB into LSB
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else return ERR_ARGS;
                    break;

                case 'r': // Register
                    if (buffer[i++] == 'r') {
                        if ((ret = scan_regval(&buffer[i], out)) <= 0) return ERR_ARGS;
                        if (*out >= 8) return ERR_ARGS;
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else return ERR_ARGS;
                    break;

                case 's': // String
                    if (buffer[i++] == '"') {
                        len = 0;
                        strp[0] = '\0';
                        while ((buffer[i] >= ' ') && (buffer[i] <= '~')) {
                            if (&strp[len] >= &str[MAX_ARG_SIZE]) return ERR_MEMORY;
                            if (buffer[i] == '\\') { // Handle escape characters
                                if ((ret = str_escape_char(&strp[len++], buffer[++i])) != ERR_NONE) return ret;
                                i++;
                            }
                            else if (buffer[i] == '"') break;
                            else strp[len++] = buffer[i++];
                        }
                        if (buffer[i++] != '"') return ERR_PARSE;
                        if (!len) return ERR_ARGS;

                        // Copy string to the output buffer
                        strp[len] = '\0';
                        *(out++) = (size_t)strp;
                        strp += (len + 1);
                    }
                    else return ERR_PARSE;
                    break;

                case 't': // Text
                    if ((buffer[i] >= ' ') && (buffer[i] <= '~')) {
                        if (buffer[i] == '\'') { // Do not allow instances of '''
                            return ERR_PARSE;
                        }
                        else if (buffer[i] == '\\') { // Handle escape characters
                            if ((ret = str_escape_char((char*)(out++), buffer[++i])) != ERR_NONE) return ret;
                            i++;
                        }
                        else *(out++) = buffer[i++]; // Advance output pointer
                    }
                    break;

                case 'w': // Whitespace
                    str_ltrim(&buffer[i], STRIP_ALL);
                    break;

                case 'P': // Register File Pair
                    if ((ret = scan_number(&buffer[i], out)) > 0) {
                        if (*out >= 0x0100) return ERR_ARGS;
                        if (*out & 1) return ERR_ARGS;
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else if ((buffer[i] == 'R') && (buffer[i + 1] == 'R')) { // FIXME: Make case insensitive?
                        i += 2;
                        if ((ret = scan_regval(&buffer[i], out)) <= 0) return ERR_ARGS;
                        if (*out & 1) return ERR_ARGS;
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else {
                        // Handle reg names
                        for (j = 0; j < (NUM_REGS / 2); j++) {
                            if (regs_w[j]) {
                                len = strlen(regs_w[j]);
                                if ((!strnicmp(&buffer[i], regs_w[j], len)) &&
                                    (!ISWORD(buffer[i + len]))) {
                                    *(out++) = (j << 1); // Advance output pointer
                                    i += len;
                                    break;
                                }
                            }
                        }
                        if (j >= (NUM_REGS / 2)) return ERR_ARGS;
                    }
                    break;

                case 'i': // Port Register
                case 'R': // Register File
                    port = (cmp[c - 1] == 'i');
                    if ((ret = scan_number(&buffer[i], out)) > 0) {
                        if (*out >= 0x0100) return ERR_ARGS;
                        if ((port) && ((*out < 16) || (*out > 23))) return ERR_ARGS;
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else if (buffer[i] == 'R') { // FIXME: Make case insensitive?
                        i++;
                        if ((ret = scan_regval(&buffer[i], out)) <= 0) return ERR_ARGS;
                        if ((port) && ((*out < 16) || (*out > 23))) return ERR_ARGS;
                        out++; // Advance output pointer
                        i += ret;
                    }
                    else {
                        // Handle reg names
                        for (j = 0; j < NUM_REGS; j++) {
                            if (regs[j]) {
                                len = strlen(regs[j]);
                                if ((!strnicmp(&buffer[i], regs[j], len)) &&
                                    (!ISWORD(buffer[i + len]))) {
                                    if ((port) && ((j < 16) || (j > 23))) return ERR_ARGS;
                                    *(out++) = j; // Advance output pointer
                                    i += len;
                                    break;
                                }
                            }
                        }
                        if (j >= NUM_REGS) return ERR_ARGS;
                    }
                    break;
            }
        }
        else if (buffer[i++] != cmp[c++]) return ERR_PARSE;
    }

    return ERR_NONE;
}


// Object handling
CHUNK *create_chunk() {
    CHUNK *chunk;

    if (!(chunk = (CHUNK*)malloc(sizeof(CHUNK)))) return NULL;
    chunk->length = 0;
    chunk->alloc = 256;
    if (!(chunk->data = (uint8_t*)malloc(chunk->alloc * sizeof(uint8_t*)))) return NULL;
    memset(chunk->data, 0, (chunk->alloc * sizeof(uint8_t*)));

    return chunk;
}

int delete_chunk(CHUNK * chunk) {
    free(chunk->data);
    free(chunk);

    return ERR_NONE;
}

int grow_chunk(CHUNK *chunk) {
    chunk->alloc += 256;
    if (!(chunk->data = (uint8_t*)realloc(chunk->data, (chunk->alloc * sizeof(uint8_t*))))) return ERR_MEMORY;

    memset(&chunk->data[chunk->alloc - 256], 0, ((chunk->alloc - 256) * sizeof(uint8_t*)));

    return ERR_NONE;
}


OBJECT *create_object() {
    OBJECT *object;

    if (!(object = (OBJECT*)malloc(sizeof(OBJECT)))) return NULL;
    object->length = 0;
    object->alloc = 256;
    if (!(object->chunks = (CHUNK**)malloc(object->alloc * sizeof(CHUNK*)))) return NULL;
    memset(object->chunks, 0, (object->alloc * sizeof(CHUNK*)));

    return object;
}

int delete_object(OBJECT *object) {
    uint32_t i;

    for (i = 0; i < object->length; i++) {
        delete_chunk(object->chunks[i]);
    }

    free(object->chunks);
    free(object);

    return ERR_NONE;
}

int grow_object(OBJECT *object) {
    object->alloc += 256;
    if (!(object->chunks = (CHUNK**)realloc(object->chunks, (object->alloc * sizeof(CHUNK*))))) return ERR_MEMORY;

    memset(&object->chunks[object->alloc - 256], 0, ((object->alloc - 256) * sizeof(CHUNK*)));

    return ERR_NONE;
}


// Main
int assemble(char *filename) {
    FILE *fin;
    STATE state;
    STATE *cur_state;

    int i;
    int j;
    int err = ERR_NONE;
    int ret;
    int opsize;
    int newline;

    char *line;
    char op[2][MAX_ARG_SIZE];
    char tmpstr[MAX_ARG_SIZE];

    uint8_t *buffer;


    // Initialize state
    cur_state = get_current_state();
    state.pc = cur_state->pc;
    strcpy(state.filename, filename);
    state.line_num = 0;
    state.last_scope[0] = '\0';
    state.last_state = cur_state;

    // Set current state
    set_current_state(&state);

    // Allocate memory
    if (!(buffer = (uint8_t*)malloc(MAX_OUTPUT))) {
        printf("Unable to allocate %d bytes of memory\n", MAX_OUTPUT);
        return ERR_MEMORY;
    }
    if (!(line = (char*)malloc(MAX_LINE))) {
        printf("Unable to allocate %d bytes of memory\n", MAX_LINE);
        free(buffer);
        return ERR_MEMORY;
    }

    // Open input file
    if (!(fin = fopen(filename, "rt"))) {
        printf("Unable to open file \"%s\" in mode \"%s\"\n", state.filename, "rt");
        free(buffer);
        free(line);
        return ERR_FILE;
    }

    // Parse each line of input
    // FIXME: This should be cleaned up!
    while (!feof(fin)) {
        newline = 0;
        state.line_num++;
        if (fgets(line, MAX_LINE, fin) > 0) {

            // Strip comments
            // FIXME: Handle C/C++ style multi-line comments
            if ((ret = str_find_parsed(line, "//")) >= 0) line[ret] = '\0';
            if ((ret = str_find_parsed(line, ";")) >= 0) line[ret] = '\0';
            str_rtrim(line, STRIP_ALL);

            // Check line length
            if (strlen(line) >= (MAX_LINE - 1)) {
                printf("Error:%s:%d: Line too long\n", state.filename, state.line_num);
                err = ERR_PARSE;
                break;
            }
            if (strlen(line) == 0) continue; // Empty line

            // Scan labels and instruction
            i = 0;
            while (1) {
                str_ltrim(&line[i], STRIP_ALL);
                sprintf(tmpstr, "%%%ds", (MAX_ARG_SIZE - 1)); // Format = "%MAX_ARG_SIZEs" Example: "%127s"
                if (sscanf(&line[i], tmpstr, op[0]) == EOF) {
                    op[0][0] = '\0';
                    break;
                }
                i += strlen(op[0]);
                if ((!ISWHITESPACE(line[i])) && (line[i] != '\0')) {
                    printf("Error:%s:%d: Instruction or label too long\n", state.filename, state.line_num);
                    err = ERR_PARSE;
                    break;
                }

                // Handle labels
                if ((ret = islabel(op[0])) != SYMTYPE_NONE) {
                    op[0][strlen(op[0]) - 1] = '\0'; // Strip trailing colon
                    switch (ret) {
                        case SYMTYPE_LABEL:
#ifdef DBG
                            printf("Info:%s:%d: adding symbol \"%s\" = 0x%08X\n", state.filename, state.line_num, op[0], state.pc);
#endif // DBG
                            strcpy(state.last_scope, op[0]);
                            err = add_symbol(&symtab, state.last_scope, SYMTYPE_LABEL, state.pc);
                            break;

                        case SYMTYPE_LOCAL_LABEL:
                            sprintf(tmpstr, "%s@%s", state.last_scope, op[0]);
#ifdef DBG
                            printf("Info:%s:%d: adding symbol \"%s\" = 0x%08X\n", state.filename, state.line_num, tmpstr, state.pc);
#endif // DBG
                            err = add_symbol(&symtab, tmpstr, SYMTYPE_LOCAL_LABEL, state.pc);
                            break;
                    }
                    if (err == ERR_NONE) continue;
                }

                break;
            }
            if (err != ERR_NONE) break;
            if (!strlen(op[0])) continue;
            if (strlen(op[0]) > MAX_INST_LEN) {
                printf("Error:%s:%d: Instruction too long\n", state.filename, state.line_num);
                err = ERR_PARSE;
                break;
            }
            str_lcase(op[0]);

            // Scan operands
            str_ltrim(&line[i], STRIP_ALL);
            if (strlen(&line[i]) >= MAX_ARG_SIZE) {
                printf("Error:%s:%d: Instruction operands too long\n", state.filename, state.line_num);
                err = ERR_ARGS;
                break;
            }
            strcpy(op[1], &line[i]);

            // Assemble instruction
            for (i = 0; i < NUM_INST; i++) {
                if (!strcmp(op[0], inst[i].name)) {
                    opsize = 0;
                    err = (*inst[i].func)(op[1], inst[i].bytecode, &opsize, buffer);

                    // FIXME: Add new error handling here
                    delete_errors();

                    if (err != ERR_NONE) {
                        printf("Error:%s:%d: (*inst[%d].func)() returned %d: %s\n", state.filename, state.line_num, i, err, err_string[err]);
                        printf("\t%s\t%s\n", op[0], op[1]);
                    }
                    else if (opsize) {
                        state.pc += opsize;
                        if (state.pc >= 0x00010000) {
                            err = ERR_MISC;
                            printf("Error:%s:%d: Memory overflow\n", state.filename, state.line_num);
                        }
                        else {
#ifdef DBG
                            for (j = 0; j < opsize; j++) {
                                if ((j) && (!(j % 4))) printf("...\n");
                                printf("%02X ", buffer[j]);
                            }
                            if ((opsize % 4) && ((opsize % 4) < 3)) printf("\t");
                            printf("\t%s\t%s\n", op[0], op[1]);
#endif // DBG
                        }
                    }
                    /*
                    // Some assembler directives do not output data
                    else {
                        err = ERR_PARSE;
                        printf("Error:%s:%d: opsize != 0\n", state.filename, state.line_num);
                    }
                    */

                    break;
                }
            }
            if (err != ERR_NONE) break;
            if (i >= NUM_INST) {
                printf("Error:%s:%d: Unknown instruction \"%s\"\n", state.filename, state.line_num, op[0]);
                err = ERR_PARSE;
                break;
            }
        }
        else newline = 1;
    }
    if ((err == ERR_NONE) && (!newline)) {
        printf("Warning:%s:%d: No new line at end of file\n", state.filename, state.line_num);
    }

    free(buffer);
    free(line);
    fclose(fin);

    // Close current state and update previous state
    state.last_state->pc = state.pc;
    set_current_state(state.last_state);

    return err;
}
