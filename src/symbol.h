/* as85 - Assembler for Sharp sm8521 CPU
 * Assembler symbol routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __SYMBOL_H__
#define __SYMBOL_H__

#include <stdint.h>


// Symbol types
#define SYMTYPE_NONE                0x0000
#define SYMTYPE_LABEL               0x0001
#define SYMTYPE_LOCAL_LABEL         0x0002
#define SYMTYPE_DEFINE              0x0003

// Symbol flags
#define SYMFLAG_GLOBAL              0x0001


typedef struct {
    char *name;         // Symbol name
    char *filename;     // Source file name
    int line_num;       // Line number in source
    uint16_t flags;
    uint16_t type;
    uint32_t id;
    uint32_t data;
} SYMBOL;

typedef struct {
    int count;
    SYMBOL *symbols;
} SYMTAB;

extern SYMTAB symtab;


// FIXME: Need functions to delete a single symbol by name/id
int delete_symbol_table(SYMTAB *table);
int add_symbol(SYMTAB *table, char *name, int type, uint32_t data);
SYMBOL *get_symbol(SYMTAB *table, char *name);
SYMBOL *get_symbol_by_id(SYMTAB *table, uint32_t id);

#endif // __SYMBOL_H__
