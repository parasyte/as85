/* as85 - Assembler for Sharp sm8521 CPU
 * Scanner routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "as85.h"
#include "error.h"
#include "scan.h"
#include "xstring.h"


// Scanners
int scan_hex(char *arg, size_t *out) {
    int i;
    int mode = 0;
    int start = 2;

    // Hex notation modes: 0 = "0x...", 1 = "...h"
    if ((arg[0] != '0') || (LCASECHAR(arg[1]) != 'x')) {
        mode = 1;
        start = 0;
    }

    *out = 0;
    for (i = start; i < (start + 32); i++) {
        if (ISNUM(arg[i])) {
            *out <<= 4;
            *out |= (arg[i] - '0');
        }
        else if ((UCASECHAR(arg[i]) >= 'A') && (UCASECHAR(arg[i]) <= 'F')) {
            *out <<= 4;
            *out |= ((UCASECHAR(arg[i]) - 'A') + 0x0A);
        }
        else break;
    }
    if (i == start) return -1;
    if (mode == 1) {
        if ((arg[i] != 'h') || (ISWORD(arg[i + 1]))) return -2;
        i++;
    }
    else if (ISWORD(arg[i])) return -3;

    return i;
}

int scan_bin(char *arg, size_t *out) {
    int i;
    int mode = 0;
    int start = 1;

    // Binary notation modes: 0 = "%...", 1 = "...b"
    if (arg[0] != '%') {
        mode = 1;
        start = 0;
    }

    *out = 0;
    for (i = start; i < (start + 32); i++) {
        if ((arg[i] >= '0') && (arg[i] <= '1')) {
            *out <<= 1;
            *out |= (arg[i] - '0');
        }
        else break;
    }
    if (i == start) return -1;
    if (mode == 1) {
        if ((arg[i] != 'b') || (ISWORD(arg[i + 1]))) return -2;
        i++;
    }
    else if (ISWORD(arg[i])) return -3;

    return i;
}

int scan_oct(char *arg, size_t *out) {
    int i;

    // Octal numbers must start with 0
    if (arg[0] != '0') return -1;

    *out = 0;
    for (i = 0; i < 10; i++) {
        if ((arg[i] >= '0') && (arg[i] <= '7')) {
            *out <<= 3;
            *out |= (arg[i] - '0');
        }
        else break;
    }
    if (i == 0) return -1;
    if (ISWORD(arg[i])) return -2;

    return i;
}

int scan_dec(char *arg, size_t *out) {
    int i;
    int neg = 0;
    int start = 0;

    // FIXME: Handle negation in expression parser
    // Handle negative numbers
    if (arg[0] == '-') {
        neg = 1;
        start++;
    }

    // Decimal numbers cannot start with 0, except 0
    if ((arg[start] == '0') && (ISNUM(arg[start + 1]))) return -1;

    *out = 0;
    for (i = start; i < (start + 10); i++) {
        if (ISNUM(arg[i])) *out = ((*out * 10) + (arg[i] - '0'));
        else break;
    }
    if (i == start) return -1;
    if (ISWORD(arg[i])) return -2;

    if (neg) {
        if (!*out) add_error("Negative zero truncated", ERR_WARNING);
        else if (*out >= 0x80000000) add_error("Negating unsigned value", ERR_WARNING);
        *out = (~*out + 1);
    }

    return i;
}

int scan_number(char *arg, size_t *out) {
    int ret;

    if ((ret = scan_hex(arg, out)) >= 0) return ret;
    if ((ret = scan_bin(arg, out)) >= 0) return ret;
    if ((ret = scan_oct(arg, out)) >= 0) return ret;
    if ((ret = scan_dec(arg, out)) >= 0) return ret;

    return -1;
}

int scan_regval(char *arg, size_t *out) {
    int i = 0;

    *out = 0;
    while (ISNUM(arg[i])) { // Don't use scan_dec() -- we want numbers beginning with 0, here
        if (i >= 3) return -1;
        *out = ((*out * 10) + (arg[i++] - '0'));
    }
    if (*out >= 256) return -1;

    return i;
}
