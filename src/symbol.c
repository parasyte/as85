/* as85 - Assembler for Sharp sm8521 CPU
 * Assembler symbol routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "as85.h"
#include "symbol.h"

SYMTAB symtab = {
    0,          // count
    NULL,       // symbols
};


int delete_symbol_table(SYMTAB *table) {
    if (table->count) {
        while (table->count--) {
            free(table->symbols[table->count].name);
            free(table->symbols[table->count].filename);
        }
        free(table->symbols);

        return ERR_NONE;
    }

    return ERR_MISC;
}

int add_symbol(SYMTAB *table, char *name, int type, uint32_t data) {
    SYMBOL *exists;
    STATE *cur_state;

    cur_state = get_current_state();

    // Check for pre-existing symbol
    if ((exists = get_symbol(table, name))) {
        printf("Error:%s:%d: Symbol \"%s\" previously defined at %s:%d\n", cur_state->filename, cur_state->line_num, exists->name, exists->filename, exists->line_num);
        return ERR_SYMBOL;
    }

    // Re-allocate memory for table symbols
    if (!(table->symbols = (SYMBOL*)realloc(table->symbols, (sizeof(SYMBOL) * (table->count + 1))))) {
        printf("Fatal error: add_symbol():");
        printf("Unable to allocate %lu bytes of memory\n", (sizeof(SYMBOL) * (table->count + 1)));
        exit(1); //return ERR_MEMORY;
    }

    // Allocate memory for name
    if (!(table->symbols[table->count].name = (char*)malloc(strlen(name) + 1))) {
        printf("Fatal error: add_symbol():");
        printf("Unable to allocate %lu bytes of memory\n", (strlen(name) + 1));
        exit(1); //return ERR_MEMORY;
    }

    // Allocate memory for filename
    if (!(table->symbols[table->count].filename = (char*)malloc(strlen(cur_state->filename) + 1))) {
        printf("Fatal error: add_symbol():");
        printf("Unable to allocate %lu bytes of memory\n", (strlen(cur_state->filename) + 1));
        exit(1); //return ERR_MEMORY;
    }

    // Copy symbol into table
    strcpy(table->symbols[table->count].name, name);
    strcpy(table->symbols[table->count].filename, cur_state->filename);
    table->symbols[table->count].line_num = cur_state->line_num;
    table->symbols[table->count].flags = 0;
    table->symbols[table->count].type = type;
    table->symbols[table->count].id = table->count; // FIXME: Needs to use get_next_symbol() or such
    table->symbols[table->count].data = data;

    // Increment symbol count
    table->count++;

    return ERR_NONE;
}

SYMBOL *get_symbol(SYMTAB *table, char *name) {
    int i;

    for (i = 0; i < table->count; i++) {
        if (!strcmp(table->symbols[i].name, name)) return &table->symbols[i];
    }

    return NULL;
}

SYMBOL *get_symbol_by_id(SYMTAB *table, uint32_t id) {
    int i;

    for (i = 0; i < table->count; i++) {
        if (table->symbols[i].id == id) return &table->symbols[i];
    }

    return NULL;
}
