/* as85 - Assembler for Sharp sm8521 CPU
 * Main program routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __AS85_H__
#define __AS85_H__

#include <stdint.h>


// Debugging
#define DBG


// Constants
#define MAX_LINE                    512
#define MAX_OUTPUT                  0x1000
#define MAX_ARG_SIZE                256
#define MAX_INST_LEN                8


// Macros
#define MIN(n, m)                   (((n) < (m)) ? (n) : (m))
#define MAX(n, m)                   (((n) > (m)) ? (n) : (m))
//#define CLIP(n, m, x)             MAX(MIN(MIN((n), (x)), MAX((n), (m))), (m))


// Error codes
#define ERR_NONE                    0
#define ERR_PARSE                   1 // Parse error
#define ERR_ARGS                    2 // Invalid args
#define ERR_ADDRMODE                3 // Invalid address mode
#define ERR_FILE                    4 // Unable to open/read/write file
#define ERR_MEMORY                  5 // Unable to allocate memory
#define ERR_SYMBOL                  6 // Pre-existing symbol
#define ERR_RESOLVE                 7 // Could not resolve symbols
#define ERR_MISC                    8 // Miscellaneous
#define ERR_WARNING                 9 // Non-fatal warnings


typedef struct _STATE {
    uint32_t pc;                         // FIXME: Move PC handling out of assembler and into linker
    char filename[128];
    int line_num;
    char last_scope[128];
    struct _STATE *last_state;
} STATE;

extern char *err_string[];

STATE *get_current_state();
void set_current_state(STATE *state);

#endif // __AS85_H__
