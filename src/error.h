/* as85 - Assembler for Sharp sm8521 CPU
 * Error and warning routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __ERROR_H__
#define __ERROR_H__

typedef struct {
    int type;
    char *msg;
    STATE state;
} ERROR_MSG;

typedef struct {
    int count;
    ERROR_MSG *msg;
} ERROR_TABLE;


ERROR_MSG *get_error(int id);
int delete_errors();
int add_error(char *msg, int type);

#endif // __ERROR_H__
