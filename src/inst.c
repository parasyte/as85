/* as85 - Assembler for Sharp sm8521 CPU
 * Sharp sm8521 instruction routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "as85.h"
#include "asm.h"
#include "inst.h"
#include "xstring.h"


const uint8_t bytecodes_type0[NUM_TYPE0] = {
    0xF0, // stop
    0xF1, // halt
    0xF8, // ret
    0xF9, // iret
    0xFA, // clrc
    0xFB, // comc
    0xFC, // setc
    0xFD, // ei
    0xFE, // di
    0xFF, // nop
};

const uint8_t bytecodes_type1[NUM_TYPE1][3] = {
    { 0x1A, 0x00, 0x00 }, // clr
    { 0x1A, 0x01, 0x01 }, // neg
    { 0x1A, 0x02, 0x02 }, // com
    { 0x1A, 0x03, 0x03 }, // rr
    { 0x1A, 0x04, 0x04 }, // rl
    { 0x1A, 0x05, 0x05 }, // rrc
    { 0x1A, 0x06, 0x06 }, // rlc
    { 0x1A, 0x07, 0x07 }, // srl
    { 0x1B, 0x00, 0x08 }, // inc
    { 0x1B, 0x01, 0x09 }, // dec
    { 0x1B, 0x02, 0x0A }, // sra
    { 0x1B, 0x03, 0x0B }, // sll
    { 0x1B, 0x04, 0x0C }, // da
    { 0x1B, 0x05, 0x0D }, // swap
    { 0x1B, 0x06, 0x0E }, // push
    { 0x1B, 0x07, 0x0F }, // pop
};

const uint8_t bytecodes_type2[NUM_TYPE2] = {
    0x00, // cmp
    0x01, // add
    0x02, // sub
    0x03, // adc
    0x04, // sbc
    0x05, // and
    0x06, // or
    0x07, // xor
};
/*
// Shares bytecodes with type2!
const uint8_t bytecodes_type3[NUM_TYPE3] = {
    0x00, // cmpw
    0x01, // addw
    0x02, // subw
    0x03, // adcw
    0x04, // sbcw
    0x05, // andw
    0x06, // orw
    0x07, // xorw
};
*/
const uint8_t bytecodes_type4[NUM_TYPE4] = {
    0x18, // incw
    0x19, // decw
    0x1E, // pushw
    0x1F, // popw
    0x2C, // exts
};

const uint8_t bytecodes_type5[NUM_TYPE5][2] = {
    { 0x1C, 0xA0 }, // bclr
    { 0x1D, 0xA8 }, // bset
};

const uint8_t bytecodes_type6[NUM_TYPE6] = {
    0x00, // bcmp
    0x40, // band
    0x80, // bor
    0xC0, // bxor
};

const uint8_t bytecodes_bmov[2] = {
    0x00, // bmov   bf,R,imm3
    0x40, // bmov   R,imm3,bf
};

const uint8_t bytecodes_btst[1] = {
    0x2F, // btst
};

const uint8_t bytecodes_div[2] = {
    0x5C, // div    RR,RR
    0x5D, // div    RR,#imm8
};

const uint8_t bytecodes_mov[7] = {
    0x08, // mov
    0x09, // mov
    0x2E, // mov    ps0,#imm8
    0xB0, // mov    r,R
    0xB8, // mov    R,r
    0xC0, // mov    r,#imm8
    0xC8, // mov    p,#imm8
};

const uint8_t bytecodes_movm[2] = {
    0x5E, // movm   R,#imm8,R
    0x5F, // movm   R,#imm8,#imm8
};

const uint8_t bytecodes_movw[4] = {
    0x0A, // movw
    0x0B, // movw
    0x0C, // movw   rr,rr
    0x78, // movw   rr,#imm16
};

const uint8_t bytecodes_mult[2] = {
    0x4C, // mult   RR,R
    0x4D, // mult   RR,#imm8
};

const uint8_t bytecodes_db[2] = {
    0, // .db
    1, // .string
};


// Limit each instruction name to maximum 5 chars
// Limit each directive name to maximum 8(?) chars
INST inst[NUM_INST] = {
    // type0
    { "stop",       inst_type0,         &bytecodes_type0[0] },
    { "halt",       inst_type0,         &bytecodes_type0[1] },
    { "ret",        inst_type0,         &bytecodes_type0[2] },
    { "iret",       inst_type0,         &bytecodes_type0[3] },
    { "clrc",       inst_type0,         &bytecodes_type0[4] },
    { "comc",       inst_type0,         &bytecodes_type0[5] },
    { "setc",       inst_type0,         &bytecodes_type0[6] },
    { "ei",         inst_type0,         &bytecodes_type0[7] },
    { "di",         inst_type0,         &bytecodes_type0[8] },
    { "nop",        inst_type0,         &bytecodes_type0[9] },

    // type1
    { "clr",        inst_type1,         &bytecodes_type1[0][0] },
    { "neg",        inst_type1,         &bytecodes_type1[1][0] },
    { "com",        inst_type1,         &bytecodes_type1[2][0] },
    { "rr",         inst_type1,         &bytecodes_type1[3][0] },
    { "rl",         inst_type1,         &bytecodes_type1[4][0] },
    { "rrc",        inst_type1,         &bytecodes_type1[5][0] },
    { "rlc",        inst_type1,         &bytecodes_type1[6][0] },
    { "srl",        inst_type1,         &bytecodes_type1[7][0] },
    { "inc",        inst_type1,         &bytecodes_type1[8][0] },
    { "dec",        inst_type1,         &bytecodes_type1[9][0] },
    { "sra",        inst_type1,         &bytecodes_type1[10][0] },
    { "sll",        inst_type1,         &bytecodes_type1[11][0] },
    { "da",         inst_type1,         &bytecodes_type1[12][0] },
    { "swap",       inst_type1,         &bytecodes_type1[13][0] },
    { "push",       inst_type1,         &bytecodes_type1[14][0] },
    { "pop",        inst_type1,         &bytecodes_type1[15][0] },

    // type2
    { "cmp",        inst_type2,         &bytecodes_type2[0] },
    { "add",        inst_type2,         &bytecodes_type2[1] },
    { "sub",        inst_type2,         &bytecodes_type2[2] },
    { "adc",        inst_type2,         &bytecodes_type2[3] },
    { "sbc",        inst_type2,         &bytecodes_type2[4] },
    { "and",        inst_type2,         &bytecodes_type2[5] },
    { "or",         inst_type2,         &bytecodes_type2[6] },
    { "xor",        inst_type2,         &bytecodes_type2[7] },

    // type3
    { "cmpw",       inst_type3,         &bytecodes_type2[0] }, // Shares bytecodes with type2!
    { "addw",       inst_type3,         &bytecodes_type2[1] },
    { "subw",       inst_type3,         &bytecodes_type2[2] },
    { "adcw",       inst_type3,         &bytecodes_type2[3] },
    { "sbcw",       inst_type3,         &bytecodes_type2[4] },
    { "andw",       inst_type3,         &bytecodes_type2[5] },
    { "orw",        inst_type3,         &bytecodes_type2[6] },
    { "xorw",       inst_type3,         &bytecodes_type2[7] },

    // type4
    { "incw",       inst_type4,         &bytecodes_type4[0] },
    { "decw",       inst_type4,         &bytecodes_type4[1] },
    { "pushw",      inst_type4,         &bytecodes_type4[2] },
    { "popw",       inst_type4,         &bytecodes_type4[3] },
    { "exts",       inst_type4,         &bytecodes_type4[4] },

    // type5
    { "bclr",       inst_type5,         &bytecodes_type5[0][0] },
    { "bset",       inst_type5,         &bytecodes_type5[1][0] },

    // type6
    { "bcmp",       inst_type6,         &bytecodes_type6[0] },
    { "band",       inst_type6,         &bytecodes_type6[1] },
    { "bor",        inst_type6,         &bytecodes_type6[2] },
    { "bxor",       inst_type6,         &bytecodes_type6[3] },

    // non-typed
    { "bmov",       inst_bmov,          &bytecodes_bmov[0] },
    { "btst",       inst_btst,          &bytecodes_btst[0] },
    { "div",        inst_div,           &bytecodes_div[0] },
    { "mov",        inst_mov,           &bytecodes_mov[0] },
    { "movm",       inst_movm,          &bytecodes_movm[0] },
    { "movw",       inst_movw,          &bytecodes_movw[0] },
    { "mult",       inst_mult,          &bytecodes_mult[0] },

    // Pseudo-instructions
//    { "blt",        pinst_???,          &bytecodes_???[0] },      // br lt
//    { "ble",        pinst_???,          &bytecodes_???[0] },      // br le
//    { "bleu",       pinst_???,          &bytecodes_???[0] },      // br ule
//    { "bvs",        pinst_???,          &bytecodes_???[0] },      // br ov
//    { "bmi",        pinst_???,          &bytecodes_???[0] },      // br mi
//    { "beq",        pinst_???,          &bytecodes_???[0] },      // br z
//    { "bcs",        pinst_???,          &bytecodes_???[0] },      // br c
//    { "b",          pinst_???,          &bytecodes_???[0] },      // br t
//    { "bge",        pinst_???,          &bytecodes_???[0] },      // br ge
//    { "bgt",        pinst_???,          &bytecodes_???[0] },      // br gt
//    { "bgtu",       pinst_???,          &bytecodes_???[0] },      // br ugt
//    { "bvc",        pinst_???,          &bytecodes_???[0] },      // br nov
//    { "bpl",        pinst_???,          &bytecodes_???[0] },      // br pl
//    { "bne",        pinst_???,          &bytecodes_???[0] },      // br nz
//    { "bcc",        pinst_???,          &bytecodes_???[0] },      // br nc

//    { "jlt",        pinst_???,          &bytecodes_???[0] },      // jmp lt
//    { "jle",        pinst_???,          &bytecodes_???[0] },      // jmp le
//    { "jleu",       pinst_???,          &bytecodes_???[0] },      // jmp ule
//    { "jvs",        pinst_???,          &bytecodes_???[0] },      // jmp ov
//    { "jmi",        pinst_???,          &bytecodes_???[0] },      // jmp mi
//    { "jeq",        pinst_???,          &bytecodes_???[0] },      // jmp z
//    { "jcs",        pinst_???,          &bytecodes_???[0] },      // jmp c
//    { "j",          pinst_???,          &bytecodes_???[0] },      // jmp t
//    { "jge",        pinst_???,          &bytecodes_???[0] },      // jmp ge
//    { "jgt",        pinst_???,          &bytecodes_???[0] },      // jmp gt
//    { "jgtu",       pinst_???,          &bytecodes_???[0] },      // jmp ugt
//    { "jvc",        pinst_???,          &bytecodes_???[0] },      // jmp nov
//    { "jpl",        pinst_???,          &bytecodes_???[0] },      // jmp pl
//    { "jne",        pinst_???,          &bytecodes_???[0] },      // jmp nz
//    { "jcc",        pinst_???,          &bytecodes_???[0] },      // jmp nc

    // Assembler directives
//    { ".align",     dinst_align,        NULL },
//    { ".bank",      dinst_bank,         NULL },   // Handle this in linker!
    { ".db",        dinst_db,           &bytecodes_db[0] },
    { ".dd",        dinst_dd,           NULL },
//    { ".def",       dinst_define,       NULL },
//    { ".define",    dinst_define,       NULL },
    { ".dw",        dinst_dw,           NULL },
//    { ".error",     dinst_error,        NULL },
//    { ".extern",    dinst_global,       NULL },
//    { ".extrn",     dinst_global,       NULL },
//    { ".global",    dinst_global,       NULL },
//    { ".globl",     dinst_global,       NULL },
//    { ".if",        dinst_if,           NULL },   // .elseif, .else, and .endif are handled by this function
//    { ".ifdef",     dinst_ifdef,        NULL },   // .endif is handled by this function
//    { ".ifndef",    dinst_ifndef,       NULL },   // .endif is handled by this function
//    { ".inc",       dinst_include,      NULL },
//    { ".incbin",    dinst_incbin,       NULL },
//    { ".include",   dinst_include,      NULL },
//    { ".org",       dinst_org,          NULL },    // Handle this in linker!
    { ".string",    dinst_db,           &bytecodes_db[1] },
//    { ".undef",     dinst_undefine,     NULL },
//    { ".undefine",  dinst_undefine,     NULL },
};


// Common instruction handler for type2 and mov
static int inst_common(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[3]; // Decoded args

    // 0010XXXX 00dddsss - XXXX rd,@rs
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_indirect, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[0]);
        out[1] = ((dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 0010XXXX 01dddsss - XXXX rd,(rs)+
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_indirect_inc, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[0]);
        out[1] = (0x40 | (dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 0010XXXX 10ddd000 nnnnnnnn - XXXX rd,@n
    // 0011XXXX 10ddd000 nnnnnnnn nnnnnnnn- XXXX rd,@nn
    if (chk_pattern(args, AMP_reg AMP_separator AMP_direct_indirect, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;

        *size = ((dargs[1] >= 0x0100) ? 4 : 3);
        out[0] = (((dargs[1] >= 0x0100) ? 0x30 : 0x20) | bytecode[0]);
        out[1] = (0x80 | (dargs[0] << 3));
        out[2] = dargs[1];
        if (dargs[1] >= 0x0100) out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    // 0010XXXX 10dddsss nnnnnnnn - XXXX rd,n(rs)
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_index, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;
        if (dargs[2] == 0) return ERR_ARGS;

        *size = 3;
        out[0] = (0x20 | bytecode[0]);
        out[1] = (0x80 | (dargs[0] << 3) | dargs[2]);
        out[2] = dargs[1];

        return ERR_NONE;
    }

    // 0010XXXX 11dddsss - XXXX rd,-(rs)
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_indirect_dec, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[0]);
        out[1] = (0xC0 | (dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 0011XXXX 00dddsss - XXXX rd,@rrs
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_pair_indirect, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = ((dargs[0] << 3) | (dargs[1] >> 1));

        return ERR_NONE;
    }

    // 0011XXXX 01dddsss - XXXX rd,(rrs)+
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_pair_indirect_inc, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0x40 | (dargs[0] << 3) | (dargs[1] >> 1));

        return ERR_NONE;
    }

    // 0011XXXX 10dddsss nnnnnnnn nnnnnnnn - XXXX rd,nn(rrs)
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_pair_index, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;
        if (dargs[2] == 0) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0x80 | (dargs[0] << 3) | (dargs[2] >> 1));
        out[2] = dargs[1];
        out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    // 0011XXXX 11dddsss - XXXX rd,-(rrs)
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_pair_indirect_dec, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0xC0 | (dargs[0] << 3) | (dargs[1] >> 1));

        return ERR_NONE;
    }

    // 0100XXXX ssssssss dddddddd - XXXX Rd,Rs
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_reg_file, dargs) == ERR_NONE) {
        *size = 3;
        out[0] = (0x40 | bytecode[0]);
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 0101XXXX nnnnnnnn dddddddd - XXXX Rd,#imm8
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 3;
        out[0] = (0x50 | bytecode[0]);
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}



// Instruction handlers
int inst_type0(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    if (strlen(args)) return ERR_ARGS;

    *size = 1;
    out[0] = bytecode[0];
    return ERR_NONE;
}

int inst_type1(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[1]; // Decoded args

    // 0001101X 00dddXXX - XXXX @rd
    if (chk_pattern(args, AMP_reg_indirect, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = bytecode[0];
        out[1] = ((dargs[0] << 3) | bytecode[1]);

        return ERR_NONE;
    }

    // 0000XXXX dddddddd - XXXX Rd
    if (chk_pattern(args, AMP_reg_file, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = bytecode[2];
        out[1] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_type2(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 00010XXX 00dddsss - XXXX rd,rs
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x10 | bytecode[0]);
        out[1] = ((dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    return inst_common(args, bytecode, size, out);
}

int inst_type3(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 01100XXX ssssssss dddddddd - XXXX RRd,RRs
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_reg_file_pair, dargs) == ERR_NONE) {
        *size = 3;
        out[0] = (0x60 | bytecode[0]);
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 01101XXX dddddddd nnnnnnnn nnnnnnnn - XXXX RRd,#imm16
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;

        *size = 4;
        out[0] = (0x68 | bytecode[0]);
        out[1] = dargs[0];
        out[2] = dargs[1];
        out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_type4(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[1]; // Decoded args

    // 0001XXXX ddddddd0 - XXXX RRd
    if (chk_pattern(args, AMP_reg_file_pair, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = bytecode[0];
        out[1] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_type5(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[3]; // Decoded args

    // 0001110X 00000bbb dddddddd - XXXX FFdd,#b
    if (chk_pattern(args, AMP_direct AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[0] < 0xFF00) return ERR_ARGS;
        if (dargs[0] >= 0x00010000) return ERR_ARGS;
        if (dargs[1] > 7) return ERR_ARGS;

        *size = 3;
        out[0] = bytecode[0];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 0001110X 00dddbbb nnnnnnnn - XXXX n(rd),#b
    if (chk_pattern(args, AMP_reg_index AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x0100) return ERR_ARGS;
        if (dargs[1] == 0) return ERR_ARGS;
        if (dargs[2] > 7) return ERR_ARGS;

        *size = 3;
        out[0] = bytecode[0];
        out[1] = ((dargs[1] << 3) | dargs[2]);
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 10100bbb dddddddd - XXXX Rd,#b
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] > 7) return ERR_ARGS;

        *size = 2;
        out[0] = (bytecode[1] | dargs[1]);
        out[1] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_type6(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 01001111 XX000nnn dddddddd - XXXX bf,Rd,#imm3
    if (chk_pattern(args, AMP_bf AMP_separator AMP_reg_file AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] > 7) return ERR_ARGS;

        *size = 3;
        out[0] = 0x4F;
        out[1] = (bytecode[0] | dargs[1]);
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}


// Non-typed instructions
int inst_bmov(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 01001110 00000nnn dddddddd - bmov bf,Rd,#imm3
    if (chk_pattern(args, AMP_bf AMP_separator AMP_reg_file AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] > 7) return ERR_ARGS;

        *size = 3;
        out[0] = 0x4E;
        out[1] = (bytecode[0] | dargs[1]);
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 01001110 01000nnn dddddddd - bmov Rd,#imm3,bf
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate AMP_separator AMP_bf, dargs) == ERR_NONE) {
        if (dargs[1] > 7) return ERR_ARGS;

        *size = 3;
        out[0] = 0x4E;
        out[1] = (bytecode[1] | dargs[1]);
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_btst(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 00101111 nnnnnnnn dddddddd - btst Rd,#imm8
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 3;
        out[0] = bytecode[0];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_div(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 01011100 ssssssss dddddddd - div RRd,RRs
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_reg_file_pair, dargs) == ERR_NONE) {
        *size = 3;
        out[0] = bytecode[0];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 01011101 nnnnnnnn dddddddd - div RRd,#imm8
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 3;
        out[0] = bytecode[1];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_mov(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[3]; // Decoded args

    // 0010XXXX 00sssddd - mov @rd,rs
    if (chk_pattern(args, AMP_reg_indirect AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[1]);
        out[1] = ((dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 0010XXXX 01sssddd - mov (rd)+,rs
    if (chk_pattern(args, AMP_reg_indirect_inc AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[1]);
        out[1] = (0x40 | (dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 0010XXXX 10sss000 nnnnnnnn - mov @n,rs
    // 0011XXXX 10sss000 nnnnnnnn nnnnnnnn - mov @nn,rs
    if (chk_pattern(args, AMP_direct_indirect AMP_separator AMP_reg, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x00010000) return ERR_ARGS;

        *size = ((dargs[0] >= 0x0100) ? 4 : 3);
        out[0] = (((dargs[0] >= 0x0100) ? 0x30 : 0x20) | bytecode[1]);
        out[1] = (0x80 | (dargs[1] << 3));
        out[2] = dargs[0];
        if (dargs[0] >= 0x0100) out[3] = (dargs[0] >> 8);

        return ERR_NONE;
    }

    // 0010XXXX 10sssddd nnnnnnnn - mov n(rd),rs
    if (chk_pattern(args, AMP_reg_index AMP_separator AMP_reg, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x0100) return ERR_ARGS;
        if (dargs[1] == 0) return ERR_ARGS;

        *size = 3;
        out[0] = (0x20 | bytecode[1]);
        out[1] = (0x80 | (dargs[2] << 3) | dargs[1]);
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 0010XXXX 11sssddd - mov -(rd),rs
    if (chk_pattern(args, AMP_reg_indirect_dec AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x20 | bytecode[1]);
        out[1] = (0xC0 | (dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 0011XXXX 00sssddd - mov @rrd,rs
    if (chk_pattern(args, AMP_reg_pair_indirect AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = ((dargs[1] << 3) | (dargs[0] >> 1));

        return ERR_NONE;
    }

    // 0011XXXX 01sssddd - mov (rrd)+,rs
    if (chk_pattern(args, AMP_reg_pair_indirect_inc AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0x40 | (dargs[1] << 3) | (dargs[0] >> 1));

        return ERR_NONE;
    }

    // 0011XXXX 10sssddd nnnnnnnn nnnnnnnn - mov nn(rrd),rs
    if (chk_pattern(args, AMP_reg_pair_index AMP_separator AMP_reg, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x00010000) return ERR_ARGS;
        if (dargs[1] == 0) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0x80 | (dargs[2] << 3) | (dargs[1] >> 1));
        out[2] = dargs[0];
        out[3] = (dargs[0] >> 8);

        return ERR_NONE;
    }

    // 0011XXXX 11sssddd - mov -(rrd),rs
    if (chk_pattern(args, AMP_reg_pair_indirect_dec AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0xC0 | (dargs[1] << 3) | (dargs[0] >> 1));

        return ERR_NONE;
    }

    // 00101110 nnnnnnnn - mov ps0,#imm8
    if (chk_pattern(args, AMP_ps0 AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x0100) return ERR_ARGS;

        *size = 2;
        out[0] = bytecode[2];
        out[1] = dargs[0];

        return ERR_NONE;
    }

    // 10110ddd ssssssss - mov rd,Rs
    if (chk_pattern(args, AMP_reg AMP_separator AMP_reg_file, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (bytecode[3] | dargs[0]);
        out[1] = dargs[1];

        return ERR_NONE;
    }

    // 10111sss dddddddd - mov Rd,rs
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_reg, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (bytecode[4] | dargs[1]);
        out[1] = dargs[0];

        return ERR_NONE;
    }

    // 11000ddd nnnnnnnn - mov rd,#imm8
    if (chk_pattern(args, AMP_reg AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 2;
        out[0] = (bytecode[5] | dargs[0]);
        out[1] = dargs[1];

        return ERR_NONE;
    }

    // 11001ppp nnnnnnnn - mov p,#imm8
    if (chk_pattern(args, AMP_port AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 2;
        out[0] = (bytecode[6] | (dargs[0] - 16));
        out[1] = dargs[1];

        return ERR_NONE;
    }

    return inst_common(args, bytecode, size, out);
}

int inst_movm(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[3]; // Decoded args

    // 01011110 dddddddd nnnnnnnn ssssssss - movm Rd,#imm8,Rs
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate AMP_separator AMP_reg_file, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 4;
        out[0] = bytecode[0];
        out[1] = dargs[0];
        out[2] = dargs[1];
        out[3] = dargs[2];

        return ERR_NONE;
    }

    // 01011111 dddddddd nnnnnnnn mmmmmmmm - movm Rd,#imm8,#imm8
    if (chk_pattern(args, AMP_reg_file AMP_separator AMP_immediate AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;
        if (dargs[2] >= 0x0100) return ERR_ARGS;

        *size = 4;
        out[0] = bytecode[1];
        out[1] = dargs[0];
        out[2] = dargs[1];
        out[3] = dargs[2];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}


int inst_movw(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[3]; // Decoded args

    // 00111010 00dddsss - movw rrd,@rrs
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_reg_pair_indirect, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = ((dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 00111011 00sssddd - movw @rrd,rrs
    if (chk_pattern(args, AMP_reg_pair_indirect AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = ((dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 00111100 00dddsss - movw rrd,rrs
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[2]);
        out[1] = ((dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 00111010 01dddsss - movw rrd,(rrs)+
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_reg_pair_indirect_inc, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0x40 | (dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 00111011 01sssddd - movw (rrd)+,rrs
    if (chk_pattern(args, AMP_reg_pair_indirect_inc AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0x40 | (dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 00111010 10dddsss nnnnnnnn nnnnnnnn - movw rrd,nn(rrs)
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_reg_pair_index, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;
        if (dargs[2] == 0) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0x80 | (dargs[0] << 3) | dargs[2]);
        out[2] = dargs[1];
        out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    // 00111010 10ddd000 nnnnnnnn nnnnnnnn - movw rrd,@nn
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_direct_indirect, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0x80 | (dargs[0] << 3));
        out[2] = dargs[1];
        out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    // 00111011 10sssddd nnnnnnnn nnnnnnnn - movw nn(rrd),rrs
    if (chk_pattern(args, AMP_reg_pair_index AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x00010000) return ERR_ARGS;
        if (dargs[1] == 0) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0x80 | (dargs[2] << 3) | dargs[1]);
        out[2] = dargs[0];
        out[3] = (dargs[0] >> 8);

        return ERR_NONE;
    }

    // 00111011 10sss000 nnnnnnnn nnnnnnnn - movw @nn,rrs
    if (chk_pattern(args, AMP_direct_indirect AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        if (dargs[0] >= 0x00010000) return ERR_ARGS;

        *size = 4;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0x80 | (dargs[1] << 3));
        out[2] = dargs[0];
        out[3] = (dargs[0] >> 8);

        return ERR_NONE;
    }

    // 00111010 11dddsss - movw rrd,(rrs)-
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_reg_pair_indirect_dec, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[0]);
        out[1] = (0xC0 | (dargs[0] << 3) | dargs[1]);

        return ERR_NONE;
    }

    // 00111011 11sssddd - movw (rrd)-,rrs
    if (chk_pattern(args, AMP_reg_pair_indirect_dec AMP_separator AMP_reg_pair, dargs) == ERR_NONE) {
        *size = 2;
        out[0] = (0x30 | bytecode[1]);
        out[1] = (0xC0 | (dargs[1] << 3) | dargs[0]);

        return ERR_NONE;
    }

    // 01001010 sssssss0 ddddddd0 - movw RRd,RRs
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_reg_file_pair, dargs) == ERR_NONE) {
        *size = 3;
        out[0] = (0x40 | bytecode[0]);
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 01001011 ddddddd0 nnnnnnnn nnnnnnnn - movw RRd,#imm16
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;

        *size = 4;
        out[0] = (0x40 | bytecode[1]);
        out[1] = dargs[0];
        out[2] = dargs[1];
        out[3] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    // 01111ddd nnnnnnnn nnnnnnnn - movw rrd,#imm16
    if (chk_pattern(args, AMP_reg_pair AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x00010000) return ERR_ARGS;

        *size = 3;
        out[0] = (bytecode[3] | dargs[0]);
        out[1] = dargs[1];
        out[2] = (dargs[1] >> 8);

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

int inst_mult(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    size_t dargs[2]; // Decoded args

    // 01001100 ssssssss ddddddd0 - mult RRd,Rs
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_reg_file, dargs) == ERR_NONE) {
        *size = 3;
        out[0] = bytecode[0];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    // 01001101 nnnnnnnn ddddddd0 - mult RRd,#imm8
    if (chk_pattern(args, AMP_reg_file_pair AMP_separator AMP_immediate, dargs) == ERR_NONE) {
        if (dargs[1] >= 0x0100) return ERR_ARGS;

        *size = 3;
        out[0] = bytecode[1];
        out[1] = dargs[1];
        out[2] = dargs[0];

        return ERR_NONE;
    }

    return ERR_ADDRMODE;
}

/*
int dinst_org(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    if (chk_pattern(args, "%n", &cur_state->pc) != ERR_NONE) return ERR_ARGS;

    if (cur_state->pc >= 0x00010000) {
        printf("Warning:%s:%d: .org destination out of range: Truncated\n", cur_state->filename, cur_state->line_num);
        cur_state->pc &= 0xFFFF;
    }

    return ERR_NONE;
}

int dinst_align(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    uint8_t fill = 0;
    int ret;
    char str[MAX_ARG_SIZE] = { 0 };
    char *ptr;
    uint32_t dargs[1]; // Decoded args

    strcpy(str, args);
    if ((ptr = strchr(str, ','))) {
        *(ptr++) = '\0';
        if ((ret = chk_pattern(ptr, "%w%n%w", dargs)) != ERR_NONE) return ret;
        if (dargs[0] >= 0x0100) {
            printf("Warning:%s:%d: .align fill value too large: Truncated\n", cur_state->filename, cur_state->line_num);
        }
        fill = dargs[0];
    }
    if (chk_pattern(str, "%w%n%w", dargs) == ERR_NONE) {
        if (dargs[0] >= 32) return ERR_ARGS;
        dargs[0] = ((1 << dargs[0]) - 1);
        *size = (((cur_state->pc + dargs[0]) & dargs[0]) ^ dargs[0]);
        memset(out, fill, *size);

        return ERR_NONE;
    }

    return ERR_PARSE;
}
*/
int dinst_db(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    int i = 0;
    int j = 0;
    int ret;
    char str[MAX_ARG_SIZE];
    size_t dargs[2]; // Decoded args

    *size = 0;
    strcpy(str, args);
    while (j >= 0) {
        if (*size > MAX_OUTPUT) return ERR_ARGS;

        // Parse arguments, taking strings into account
        if ((j = str_find_parsed(&str[i], ",")) >= 0) {
            j += i;
            str[j++] = '\0';
            str_ltrim(&str[j], STRIP_ALL);
        }

        // Scan for numbers
        if ((ret = chk_pattern(&str[i], "%w%n%w", dargs)) == ERR_NONE) {
            if (dargs[0] >= 0x0100) return ERR_ARGS; // FIXME: Make warning
            out[(*size)++] = dargs[0];
        }

        // Scan for strings
        else if ((ret = chk_pattern(&str[i], "%w%s%w", dargs)) == ERR_NONE) {
            if ((*size + strlen((char*)dargs[0]) + (*bytecode ? 1 : 0)) > MAX_OUTPUT) return ERR_ARGS;

            strcpy((char*)&out[*size], (char*)dargs[0]);

            *size += strlen((char*)dargs[0]);
            if (*bytecode) (*size)++;
        }

        // Scan for characters
        else if ((ret = chk_pattern(&str[i], "%w'%t'%w", dargs)) == ERR_NONE) {
            out[(*size)++] = dargs[0];
        }

        if (ret != ERR_NONE) return ret;

        i = j;
    }

    return ERR_NONE;
}

int dinst_dw(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    int i;
    int num_sep = 0;
    char fmt[MAX_ARG_SIZE] = { 0 };
    char *ptr = args;
    size_t dargs[MAX_OUTPUT / 2]; // Decoded args

    // Unused
    (void)bytecode;

    // Build format string
    while ((ptr = strchr(ptr, ','))) {
        num_sep++;
        ptr++;
    }
    if (num_sep >= (MAX_OUTPUT / 2)) return ERR_ARGS;
    for (i = 0; i < num_sep; i++) {
        strcat(fmt, "%n");
        strcat(fmt, AMP_separator);
    }
    strcat(fmt, "%n");
    num_sep++;

    *size = num_sep * 2;
    if (chk_pattern(args, fmt, dargs) != ERR_NONE) return ERR_ARGS;
    for (i = 0; i < num_sep; i++) {
        if (dargs[i] >= 0x00010000) return ERR_ARGS;
        out[(i * 2) + 0] = (dargs[i] >> 0);
        out[(i * 2) + 1] = (dargs[i] >> 8);
    }

    return ERR_NONE;
}

int dinst_dd(char *args, const uint8_t *bytecode, int *size, uint8_t *out) {
    int i;
    int num_sep = 0;
    char fmt[MAX_ARG_SIZE] = { 0 };
    char *ptr = args;
    size_t dargs[MAX_OUTPUT / 4]; // Decoded args

    // Unused
    (void)bytecode;

    // Build format string
    while ((ptr = strchr(ptr, ','))) {
        num_sep++;
        ptr++;
    }
    if (num_sep >= (MAX_OUTPUT / 4)) return ERR_ARGS;
    for (i = 0; i < num_sep; i++) {
        strcat(fmt, "%n");
        strcat(fmt, AMP_separator);
    }
    strcat(fmt, "%n");
    num_sep++;

    *size = num_sep * 4;
    if (chk_pattern(args, fmt, dargs) != ERR_NONE) return ERR_ARGS;
    for (i = 0; i < num_sep; i++) {
        out[(i * 4) + 0] = (dargs[i] >> 0);
        out[(i * 4) + 1] = (dargs[i] >> 8);
        out[(i * 4) + 2] = (dargs[i] >> 16);
        out[(i * 4) + 3] = (dargs[i] >> 24);
    }

    return ERR_NONE;
}
