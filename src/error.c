/* as85 - Assembler for Sharp sm8521 CPU
 * Error and warning routines
 *
 * Copyright notice for this file:
 *  Copyright (C) 2006, 2009 Jason Oster Parasyte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "as85.h"
#include "error.h"


static ERROR_TABLE error = {
    0,          // count
    NULL,       // msg
};


ERROR_MSG *get_error(int id) {
    if (id >= error.count) return NULL;
    return &error.msg[id];
}

int delete_errors() {
    if (error.count) {
        while (error.count--) {
            free(error.msg[error.count].msg);
        }
        free(error.msg);

        return ERR_NONE;
    }

    return ERR_MISC;
}

int add_error(char *msg, int type) {
    STATE *cur_state;

    // Reallocate memory for error messages
    if (!(error.msg = (ERROR_MSG*)realloc(error.msg, (sizeof(ERROR_MSG) * (error.count + 1))))) {
        printf("Fatal error: add_error():");
        printf("Unable to allocate %lu bytes of memory\n", (sizeof(ERROR_MSG) * (error.count + 1)));
        exit(1); //return ERR_MEMORY;
    }

    // Allocate memory for message
    if (!(error.msg[error.count].msg = (char*)malloc(strlen(msg)))) {
        printf("Fatal error: add_error():");
        printf("Unable to allocate %zu bytes of memory\n", strlen(msg));
        exit(1); //return ERR_MEMORY;
    }

    // Copy error message
    cur_state = get_current_state();
    error.msg[error.count].type = type;
    strcpy(error.msg[error.count].msg, msg);
    memcpy(&error.msg[error.count].state, cur_state, sizeof(STATE));

    // Increment error count
    error.count++;

    return ERR_NONE;
}
