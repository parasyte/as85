
// This assembly file demonstrates all available instructions and address modes
// Use it to verify the assembler is outputting the proper bytecode for each

	// 0x00 - 0x0F
	clr	R1
	neg	R1
	com	R1
	rr	R1
	rl	R1
	rrc	R1
	rlc	R1
	srl	R1
	inc	R1
	dec	R1
	sra	R1
	sll	R1
	da	R1
	swap	R1
	push	R1
	pop	R1

	// 0x10 - 0x1F
	cmp	r1, r2
	add	r1, r2
	sub	r1, r2
	adc	r1, r2
	sbc	r1, r2
	and	r1, r2
	or	r1, r2
	xor	r1, r2
	incw	RR2
	decw	RR2
	clr	@r1
	neg	@r1
	com	@r1
	rr	@r1
	rl	@r1
	rrc	@r1
	rlc	@r1
	srl	@r1
	inc	@r1
	dec	@r1
	sra	@r1
	sll	@r1
	da	@r1
	swap	@r1
	push	@r1
	pop	@r1
	bclr	0xFF24, #7
	bclr	0x94(r1), #7
	bset	0xFF24, #7
	bset	0x94(r1), #7
	pushw	RR2
	popw	RR2

	// 0x20 - 0x29
	cmp	r1, @r2
	cmp	r1, (r2)+
	cmp	r1, @0x94
	cmp	r1, 0x94(r2)
	cmp	r1, -(r2)
	add	r1, @r2
	add	r1, (r2)+
	add	r1, @0x94
	add	r1, 0x94(r2)
	add	r1, -(r2)
	sub	r1, @r2
	sub	r1, (r2)+
	sub	r1, @0x94
	sub	r1, 0x94(r2)
	sub	r1, -(r2)
	adc	r1, @r2
	adc	r1, (r2)+
	adc	r1, @0x94
	adc	r1, 0x94(r2)
	adc	r1, -(r2)
	sbc	r1, @r2
	sbc	r1, (r2)+
	sbc	r1, @0x94
	sbc	r1, 0x94(r2)
	sbc	r1, -(r2)
	and	r1, @r2
	and	r1, (r2)+
	and	r1, @0x94
	and	r1, 0x94(r2)
	and	r1, -(r2)
	or	r1, @r2
	or	r1, (r2)+
	or	r1, @0x94
	or	r1, 0x94(r2)
	or	r1, -(r2)
	xor	r1, @r2
	xor	r1, (r2)+
	xor	r1, @0x94
	xor	r1, 0x94(r2)
	xor	r1, -(r2)
	mov	r1, @r2
	mov	r1, (r2)+
	mov	r1, @0x94
	mov	r1, 0x94(r2)
	mov	r1, -(r2)
	mov	@r1, r2
	mov	(r1)+, r2
	mov	@0x94, r2
	mov	0x94(r1), r2
	mov	-(r1), r2

	// 0x2A - 0x2B (unsupported)
//	bbc	0xFF24, #7, start
//	bbc	0x94(r1), #7, start
//	bbs	0xFF24, #7, start
//	bbs	0x94(r1), #7, start

	// 0x2C - 0x2F  (0x2D unavailable)
	exts	RR2
	mov	ps0, #0x94
	btst	R1, #0x94

	// 0x30 - 0x3C  (0x3D unavailable)
	cmp	r1, @rr2
	cmp	r1, (rr2)+
	cmp	r1, @0x9424
	cmp	r1, 0x9424(rr2)
	cmp	r1, -(rr2)
	add	r1, @rr2
	add	r1, (rr2)+
	add	r1, @0x9424
	add	r1, 0x9424(rr2)
	add	r1, -(rr2)
	sub	r1, @rr2
	sub	r1, (rr2)+
	sub	r1, @0x9424
	sub	r1, 0x9424(rr2)
	sub	r1, -(rr2)
	adc	r1, @rr2
	adc	r1, (rr2)+
	adc	r1, @0x9424
	adc	r1, 0x9424(rr2)
	adc	r1, -(rr2)
	sbc	r1, @rr2
	sbc	r1, (rr2)+
	sbc	r1, @0x9424
	sbc	r1, 0x9424(rr2)
	sbc	r1, -(rr2)
	and	r1, @rr2
	and	r1, (rr2)+
	and	r1, @0x9424
	and	r1, 0x9424(rr2)
	and	r1, -(rr2)
	or	r1, @rr2
	or	r1, (rr2)+
	or	r1, @0x9424
	or	r1, 0x9424(rr2)
	or	r1, -(rr2)
	xor	r1, @rr2
	xor	r1, (rr2)+
	xor	r1, @0x9424
	xor	r1, 0x9424(rr2)
	xor	r1, -(rr2)
	mov	r1, @rr2
	mov	r1, (rr2)+
	mov	r1, @0x9424
	mov	r1, 0x9424(rr2)
	mov	r1, -(rr2)
	mov	@rr2, r4
	mov	(rr2)+, r4
	mov	@0x9424, r4
	mov	0x9424(rr2), r4
	mov	-(rr2), r4
	movw	rr2, @rr4
	movw	rr2, (rr4)+
	movw	rr2, @0x9424
	movw	rr2, 0x9424(rr4)
	movw	rr2, -(rr4)
	movw	@rr2, rr4
	movw	(rr2)+, rr4
	movw	@0x9424, rr4
	movw	0x9424(rr2), rr4
	movw	-(rr2), rr4
	movw	rr2, rr4

	// 0x3E - 0x3F (unsupported)
//	jmp	rr2
//	jmp	@0x9424
//	jmp	0x9424(rr2)
//	call	rr
//	call	@0x9424
//	call	0x9424(rr2)

	// 0x40 - 0x4F
	cmp	R1, R2
	add	R1, R2
	sub	R1, R2
	adc	R1, R2
	sbc	R1, R2
	and	R1, R2
	or	R1, R2
	xor	R1, R2
	mov	R1, R2
//	call	0x9424		// 0x49 (unsupported)
	movw	RR2, RR4
	movw	RR2, #0x9424
	mult	RR2, R4
	mult	RR2, #0x94
	bmov	bf, R1, #7	//bmov	R1, #7, bf ;???
	bmov	R1, #7, bf	//bmov	bf, R1, #7 ;???
	bcmp	bf, R1, #7
	band	bf, R1, #7
	bor	bf, R1, #7	//bor	R1, #7, bf ;???
	bxor	bf, R1, #7	//bxor	R1, #7, bf ;???

	// 0x50 - 0x58  (0x59 - 0x5B unavailable)
	cmp	R1, #0x94
	add	R1, #0x94
	sub	R1, #0x94
	adc	R1, #0x94
	sbc	R1, #0x94
	and	R1, #0x94
	or	R1, #0x94
	xor	R1, #0x94
	mov	R1, #0x94

	// 0x5C - 0x5F
	div	RR2, RR4
	div	RR2, #0x94
	movm	R1, #0x94, R2
	movm	R1, #0x94, #0x24

	// 0x60 - 0x6F
	cmpw	RR2, RR4
	addw	RR2, RR4
	subw	RR2, RR4
	adcw	RR2, RR4
	sbcw	RR2, RR4
	andw	RR2, RR4
	orw	RR2, RR4
	xorw	RR2, RR4
	cmpw	RR2, #0x9424
	addw	RR2, #0x9424
	subw	RR2, #0x9424
	adcw	RR2, #0x9424
	sbcw	RR2, #0x9424
	andw	RR2, #0x9424
	orw	RR2, #0x9424
	xorw	RR2, #0x9424

	// 0x70 - 0x77 (unsupported)
//	dbnz	r0, start
//	dbnz	r1, start
//	dbnz	r2, start
//	dbnz	r3, start
//	dbnz	r4, start
//	dbnz	r5, start
//	dbnz	r6, start
//	dbnz	r7, start

	// 0x78 - 0x7F
	movw	rr0, #0x9424
	movw	rr8, #0x9424
	movw	rr2, #0x9424
	movw	rr10, #0x9424
	movw	rr4, #0x9424
	movw	rr12, #0x9424
	movw	rr6, #0x9424
	movw	rr14, #0x9424

	// 0x80 - 0x8F (unsupported)
//	bbc	R1, #0, start
//	bbc	R1, #1, start
//	bbc	R1, #2, start
//	bbc	R1, #3, start
//	bbc	R1, #4, start
//	bbc	R1, #5, start
//	bbc	R1, #6, start
//	bbc	R1, #7, start
//	bbs	R1, #0, start
//	bbs	R1, #1, start
//	bbs	R1, #2, start
//	bbs	R1, #3, start
//	bbs	R1, #4, start
//	bbs	R1, #5, start
//	bbs	R1, #6, start
//	bbs	R1, #7, start

	// 0x90 - 0x9F (unsupported)
//	jmp	f, start
//	jmp	lt, start
//	jmp	le, start
//	jmp	ule, start
//	jmp	ov, start
//	jmp	mi, start
//	jmp	z, start
//	jmp	c, start
//	jmp	t, start
//	jmp	ge, start
//	jmp	gt, start
//	jmp	ugt, start
//	jmp	nov, start
//	jmp	pl, start
//	jmp	nz, start
//	jmp	nc, start

	// 0xA0 - 0xAF
	bclr	R1, #0
	bclr	R1, #1
	bclr	R1, #2
	bclr	R1, #3
	bclr	R1, #4
	bclr	R1, #5
	bclr	R1, #6
	bclr	R1, #7
	bset	R1, #0
	bset	R1, #1
	bset	R1, #2
	bset	R1, #3
	bset	R1, #4
	bset	R1, #5
	bset	R1, #6
	bset	R1, #7

	// 0xB0 - 0xBF
	mov	r0, R1
	mov	r1, R1
	mov	r2, R1
	mov	r3, R1
	mov	r4, R1
	mov	r5, R1
	mov	r6, R1
	mov	r7, R1
	mov	R1, r0
	mov	R1, r1
	mov	R1, r2
	mov	R1, r3
	mov	R1, r4
	mov	R1, r5
	mov	R1, r6
	mov	R1, r7

	// 0xC0 - 0xCF
	mov	r0, #0x94
	mov	r1, #0x94
	mov	r2, #0x94
	mov	r3, #0x94
	mov	r4, #0x94
	mov	r5, #0x94
	mov	r6, #0x94
	mov	r7, #0x94
	mov	ie0, #0x94
	mov	ie1, #0x94
	mov	ir0, #0x94
	mov	ir1, #0x94
	mov	p0, #0x94
	mov	p1, #0x94
	mov	p2, #0x94
	mov	p3, #0x94

	// 0xD0 - 0xDF (unsupported)
//	br	f, start
//	br	lt, start
//	br	le, start
//	br	ule, start
//	br	ov, start
//	br	mi, start
//	br	z, start
//	br	c, start
//	br	t, start
//	br	ge, start
//	br	gt, start
//	br	ugt, start
//	br	nov, start
//	br	pl, start
//	br	nz, start
//	br	nc, start

	// 0xE0 - 0xEF (unsupported)
//	cals	0x1000
//	cals	0x1100
//	cals	0x1200
//	cals	0x1300
//	cals	0x1400
//	cals	0x1500
//	cals	0x1600
//	cals	0x1700
//	cals	0x1800
//	cals	0x1900
//	cals	0x1A00
//	cals	0x1B00
//	cals	0x1C00
//	cals	0x1D00
//	cals	0x1E00
//	cals	0x1F00

	// 0xF0 - 0xF2  (0xF3 - 0xF7 unavailable)
	stop
	halt

	// 0xF8 - 0xFF
	ret
	iret
	clrc
	comc
	setc
	ei
	di
	nop
