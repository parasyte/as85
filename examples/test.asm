
// .org 0x8000

	// Game.com Header
	.db	0x04			// ROM Size?
	.db	0x00			// Entry Point Bank Number
	.dw	0x0020			// Entry Point
	.db	0x00			// Unknown
	.db	"TigerDMGC"		// System Identifier: Tiger Dot Matrix Game.com
	.db	0x00			// Icon Bank Number
	.db	0x00			// Icon X-coordinate
	.db	0x00			// Icon Y-coordinate
	.db	"GCom Demo"		// Game Title
	.dw	0x0000			// Game ID
	.db	0xA5			// Security Code
//	.align	5			// Padding


start:
//	br	t, start
//	b	start

	movw	sp, #0x0100
	movw	0xBE, sp
	movw	sp, 0xBE


	clr	@r0 		; test!
test_label:
	clr	0x00
	clr	R0
	stop			;11110000
	halt			;11110001
1:	ret			;11111000
	iret			;11111001
	clrc			;11111010
	comc			;11111011
	setc			;11111100
	ei			;11111101
	di			;11111110
	nop			;11111111
	neg	R0		;00000001 RRRRRRRR
	com	R1		;00000010 RRRRRRRR
	rr	R2		;00000011 RRRRRRRR
	rl	R3		;00000100 RRRRRRRR
	rrc	@r4		;00000101 RRRRRRRR
	rlc	R5		;00000110 RRRRRRRR
	srl	@r6		;00000111 RRRRRRRR
	inc	@r7		;00001000 RRRRRRRR
	dec	R8		;00001001 RRRRRRRR
	sra	R9		;00001010 RRRRRRRR
	sll	R10		;00001011 RRRRRRRR
	da	R11		;00001100 RRRRRRRR
	swap	R12		;00001101 RRRRRRRR
	push	R13		;00001110 RRRRRRRR
	pop	R14		;00001111 RRRRRRRR
	xor	r4, r3
	cmp	r1, @r2

	incw	RR0
	pushw	RR0
	popw	RR4
	decw	RR4

	add	r0, (r4)+
	sbc	r3, -(r2)
	and	r0, 0xA2(r1)
	and	r1, 12(r7)
	sub	r0, @r1

	and	r0, 0x01A2(rr2)
	cmp	R1, R84
	or	R7, #0x94
	
	add	r4, @0x0080
	add	r4, @0x0180

	BCLR	R2, #3
	bset	0(r2), #1
	bclr	0xFF01, #7

	mov	r0, #4
	cmp	R0, #0x8A
	mov	0x0000(rr2), r0

//	.align	5, 0xFF
